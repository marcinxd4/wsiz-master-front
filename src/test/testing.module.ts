import { NgModule } from '@angular/core';
import { DummyTableComponent } from './dummy/dummy-table-component.mock';

@NgModule({
  declarations: [DummyTableComponent]
})
export class TestingModule {}
