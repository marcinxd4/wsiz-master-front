import { IState } from 'src/app/core/interface/redux.interface';
import { authInitState } from 'src/app/features/login/redux';
import { initCharmsState } from 'src/app/features/category/redux/charms.reducer';
import { initStateCatProducts } from 'src/app/core/reducer/category-list.reducer';
import { initCategoryProductState } from 'src/app/features/category/redux/product-category.reducer';
import { basketInitState } from 'src/app/features/basket/redux/state';
import { initialState as shipmentInit } from 'src/app/core/reducer/shipment.reducer';
import { initAddress } from 'src/app/core/reducer/adddress.reducer';
import { initProfileState } from 'src/app/features/profile/redux/state';
import { provideMockStore, MockStore } from '@ngrx/store/testing';

export const initalAppState: IState = {
  authentication: authInitState,
  charms: initCharmsState,
  categories: initStateCatProducts,
  products: initCategoryProductState,
  basket: basketInitState,
  shipment: shipmentInit,
  address: initAddress,
  profile: initProfileState
};

export const mockStore = provideMockStore({ initialState: initalAppState });
