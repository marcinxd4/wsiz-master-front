import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'productCharmsPrice'
})
export class ProductCharmsPricePipe implements PipeTransform {
  transform(value: any, charms: any[], amount: number): number {
    const properCharms = charms.filter(v => !v.forFree);

    if (value.discount !== null) {
      const charmsPrice = properCharms
        .filter(c => !c.forFree)
        .reduce((sum, item) => sum + (item.charm.price - item.charm.price * (value.discount.percentValue / 100)), 0);

      return (value.price - value.price * (value.discount.percentValue / 100) + charmsPrice) * amount;
    } else {
      const charmsPriceWithoutDiscount = properCharms
        .filter(c => !c.forFree)
        .reduce((sum, item) => sum + item.charm.price, 0);

      return (value.price + charmsPriceWithoutDiscount) * amount;
    }
  }
}
