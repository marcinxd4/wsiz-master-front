import { ActivatedRoute, Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { CharmsComponent } from './charms/charms.component';
import { MatDialog } from '@angular/material/dialog';
import { Subject, Observable } from 'rxjs';
import { removeCharmFromProductAction, sortCharmInProductAction, updateDetailsAction } from '../redux/charms.actions';
import { IState, IProduct, IProductCharm } from '../../../core/interface/redux.interface';
import { environment } from '../../../../environments/environment';
import { SortablejsOptions } from 'ngx-sortablejs';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { addProductToBasketAction, saveEditedProductToBasketAction } from '../../basket/redux/actions';
import { editedCharmsSelector, editedProductSelector, getEditedProductData } from '../redux/category-products.selector';
import { editModeProductAction } from '../redux/category-products.actions';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-product-view',
  templateUrl: './product-view.component.html',
  styleUrls: ['./product-view.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProductViewComponent implements OnInit, OnDestroy {
  public editMode = false;
  selectedCharms = null;
  amountPattern = /[0-9]/;
  constructor(
    private store: Store<IState>,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
    private activedRouter: ActivatedRoute,
    public dialog: MatDialog
  ) {
    this.detailsForm = this.fb.group({
      amount: [1, [Validators.min(1), Validators.max(999)]],
      comment: ['']
    });
  }
  public visibleColumn = true;
  public product$: Observable<IProduct> = this.store.pipe(select(editedProductSelector));
  public productCharms$: Observable<IProductCharm[]> = this.store.pipe(select(editedCharmsSelector));
  public detailsForm: FormGroup;

  public product: any;
  public storableEventOptions: SortablejsOptions = {
    onUpdate: (event: any) => {
      const oldDraggableIndex = event.oldDraggableIndex;
      const newDraggableIndex = event.newDraggableIndex;

      this.store.dispatch(sortCharmInProductAction({ oldDraggableIndex, newDraggableIndex }));
    }
  };

  public activeIndex = 0;

  public readonly host: string = environment.HOST;

  private unsubscribe$: Subject<void> = new Subject<void>();

  public pictureChange(index: number): void {
    this.activeIndex = index;
  }

  public openCharmsList(): void {
    const dialogRef = this.dialog.open(CharmsComponent, { width: '750px' });
    dialogRef.updatePosition({ right: '10px' });
    dialogRef.updateSize('450px');
  }

  public removeCharm(charmId: string): void {
    this.store.dispatch(removeCharmFromProductAction({ id: charmId }));
  }

  public addToBasket(): void {
    this.store.dispatch(addProductToBasketAction());
  }

  public toggleVisibleColumn(): void {
    this.visibleColumn = !this.visibleColumn;
  }

  public increaseAmount(): void {
    const current = this.detailsForm.get('amount').value;
    // tslint:disable-next-line: restrict-plus-operands
    const newValue = current + 1;
    this.detailsForm.get('amount').setValue(newValue);
  }

  public decreaseAmount(): void {
    const current = this.detailsForm.get('amount').value;
    if (current > 1) {
      this.detailsForm.get('amount').setValue(current - 1);
    }
  }
  public saveEditedProductToBasket(): void {
    this.store.dispatch(saveEditedProductToBasketAction());
  }

  ngOnInit(): void {
    // tslint:disable-next-line: newline-per-chained-call
    this.detailsForm.valueChanges.pipe(takeUntil(this.unsubscribe$)).subscribe(({ amount, comment }) => {
      this.store.dispatch(updateDetailsAction({ amount, comment }));
    });

    this.editMode = !!this.activatedRoute.snapshot.queryParamMap.get('edit');
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
