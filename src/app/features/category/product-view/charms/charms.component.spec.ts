import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CharmsComponent } from './charms.component';
import { MaterialModule } from 'src/app/core/material.module';
import { mockStore } from 'src/test/mockstore';

describe('CharmsComponent', () => {
  let component: CharmsComponent;
  let fixture: ComponentFixture<CharmsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CharmsComponent],
      imports: [MaterialModule],
      providers: [mockStore]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CharmsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
