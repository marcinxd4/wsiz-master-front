import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Store, State, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { addCharmToProductAction, getCharmsAction, removeCharmFromProductAction } from '../../redux/charms.actions';
import { environment } from '../../../../../environments/environment';
import { getAllCharmsSelector, charmsLoading } from '../../redux/charms.selectors';
import { ICharm, ICharmCategory, IState } from '../../../../core/interface/redux.interface';

@Component({
  selector: 'app-charms',
  templateUrl: './charms.component.html',
  styleUrls: ['./charms.component.scss'],
})
export class CharmsComponent implements OnInit {
  public host = environment.HOST;
  public charms$: Observable<ICharmCategory[]> = this.store.pipe(select(getAllCharmsSelector));
  public loading$ = this.store.pipe(select(charmsLoading))

  color = 'primary';
  mode = 'determinate';
  value = 50;

  constructor(
    private store: Store<IState>,
    public dialogRef: MatDialogRef<CharmsComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
  }

  ngOnInit(): void {
    this.store.dispatch(getCharmsAction());
  }

  public removeCharmFromProduct(id: string): void {
    this.store.dispatch(removeCharmFromProductAction({id}));
  }

  public addCharmToProduct(charm: ICharm): void {
    this.store.dispatch(addCharmToProductAction({charm}));
  }
}
