import { MaterialModule } from 'src/app/core/material.module';
import { NgModule } from '@angular/core';
import { MatButton, MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatDialogModule } from '@angular/material/dialog';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { NgxImageZoomModule } from 'ngx-image-zoom';
import { EffectsModule } from '@ngrx/effects';

import { CategoryComponent } from './category/category.component';
import { ProductViewComponent } from './product-view/product-view.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { CharmsComponent } from './product-view/charms/charms.component';
import { CategoryProductsEffects } from './redux/category.effects';
import { SharedModule } from '../../shared/shared.module';
import { SortablejsModule } from 'ngx-sortablejs';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { SidebarComponent } from './category/sidebar/sidebar.component';
import { CategoryPageComponent } from './category-page/category-page.component';
import { ProductCharmsPricePipe } from './product-view/product-charms-price.pipe';

const routes: Routes = [
  {
    path: '',
    component: CategoryPageComponent,
    children: [
      {
        path: ':name',
        component: CategoryComponent
      },
      {
        path: ':id/product/:product',
        component: ProductViewComponent
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    RouterModule.forChild(routes),
    EffectsModule.forFeature([CategoryProductsEffects]),
    SortablejsModule.forRoot({ animation: 150 }),
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
    MatDialogModule,
    MatButtonModule,
    MatCardModule,
    MatInputModule,
    MatFormFieldModule,
    MatInputModule,
    MatExpansionModule,
    NgxImageZoomModule,
    MatProgressSpinnerModule
  ],
  declarations: [
    ProductCharmsPricePipe,
    CategoryComponent,
    ProductViewComponent,
    CharmsComponent,
    SidebarComponent,
    CategoryPageComponent
  ],
  entryComponents: [CharmsComponent]
})
export class CategoryModule {}
