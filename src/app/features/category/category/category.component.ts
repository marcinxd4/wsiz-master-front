import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { distinctUntilChanged } from 'rxjs/operators';
import { getProductsByCategoryIdAction, editModeProductAction } from '../redux/category-products.actions';
import { IProduct, IState } from 'src/app/core/interface/redux.interface';
import { productsSelector, productsLoadingSelector } from '../redux/category-products.selector';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit, OnDestroy {
  subscription: Subscription;
  public products$: Observable<any[]> = this.store.pipe(select(productsSelector));
  public loading$: Observable<boolean> = this.store.pipe(select(productsLoadingSelector));

  constructor(private store: Store<IState>, private activateRoute: ActivatedRoute, private router: Router) {}

  public imagePath(imageData: any[]): string {
    return imageData.length ? `https://stagging.streetwood.pl/${imageData[0].imageUrl}` : 'assets/images/sample.PNG';
  }

  public imagePathCorrect(event: any, images: any[]): void {
    event.target.style = 'display: none;';
  }

  public addProductToBasket(product: IProduct): void {
    // this.store.dispatch(editProductAction({product}));
    // this.router.navigate(['/', 'category', product.productCategoryId, 'product', product.id]);
  }

  public editProduct(productId): void {
    this.store.dispatch(editModeProductAction({ productId }));
  }

  ngOnInit(): void {
    this.subscription = this.activateRoute.queryParamMap.pipe(distinctUntilChanged()).subscribe(routeParam => {
      const category = routeParam.get('categoryId');
      this.store.dispatch(getProductsByCategoryIdAction({ category }));
    });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
