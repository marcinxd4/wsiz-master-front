import { Component, OnInit } from "@angular/core";
import { Store, select } from "@ngrx/store";
// tslint:disable-next-line: no-implicit-dependencies
import { IState } from "src/app/core/interface/redux.interface";
import { Observable } from "rxjs";
import { categoriesListSelecor } from "src/app/core/selectors/categories-list.selector";

@Component({
  selector: "app-sidebar",
  templateUrl: "./sidebar.component.html",
  styleUrls: ["./sidebar.component.scss"]
})
export class SidebarComponent {
  public categoriesList$: Observable<any> = this.store.pipe(select(categoriesListSelecor));

  constructor(private store: Store<IState>) {}
}
