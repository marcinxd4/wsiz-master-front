import { Action, createAction, props } from '@ngrx/store';
import { ICharm, ICharmCategory } from '../../../core/interface/redux.interface';

export const addCharmToProductAction = createAction('[AddCharm] Add charm to product', props<{ charm: ICharm }>());
export const addCharmToProductSuccessAction = createAction('[AddCharm] Add charm to product Success', props<{ charm: ICharm }>());
export const addCharmToProductFailureAction = createAction('[AddCharm] Add charm to product failure', props<{ error: string }>());
export const removeCharmFromProductAction = createAction('[Remove Charm] remove charm from product', props<{ id: string }>());
export const sortCharmInProductAction = createAction('[Sort charm] sortCharm in product ',
  props<{ newDraggableIndex: number, oldDraggableIndex: number }>());
export const updateDetailsAction = createAction('[EditedProduct] update details', props<{ amount: number, comment: string  }>());

export const getCharmsAction = createAction('[GET CHARMS] get all charms action');
export const getCharmsSuccessAction = createAction('[GET CHARMS] get all charms success action', props<{ charms: ICharmCategory[] }>());
export const getCharmsFailureAction = createAction('[GET CHARMS] get all charms failure action', props<{ error: string }>());
