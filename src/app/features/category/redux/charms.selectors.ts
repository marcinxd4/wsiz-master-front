import { IState } from '../../../core/interface/redux.interface';
import { createSelector } from '@ngrx/store';

export const charmsFeature = (state: IState) => state.charms;

export const getAllCharmsSelector = createSelector(
  charmsFeature,
  details => details.charms
);

export const charmsError = createSelector(
  charmsFeature,
  details => details.error
);

export const charmsLoading = createSelector(
  charmsFeature,
  details => details.loading
);
