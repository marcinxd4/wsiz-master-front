import {
  getProductsByCategoryIdSuccessAction,
  getProductsByCategoryIdAction,
  getProductsByCategoryIdFailureAction,
  editModeProductSuccessAction
} from './category-products.actions';
import {
  addCharmToProductAction,
  addCharmToProductSuccessAction,
  removeCharmFromProductAction,
  sortCharmInProductAction,
  updateDetailsAction
} from './charms.actions';
import { IProducts } from '../../../core/interface/redux.interface';
import { moveArrayItem } from '../../../core/utils/array';
import { addProductToBasketSuccessAction, editProductInBasketSuccessAction } from '../../basket/redux/actions';

export const initCategoryProductState: IProducts = {
  products: [],
  edited: {
    product: null,
    charms: [],
    comment: null,
    amount: 1
  },
  error: null,
  loading: false
};

export function categoryProductsReducer(state = initCategoryProductState, action): IProducts {
  switch (action.type) {
    case getProductsByCategoryIdAction.type:
      return {
        ...state,
        error: null,
        loading: true
      };
    case getProductsByCategoryIdSuccessAction.type:
      const { products } = action;

      return {
        ...state,
        loading: false,
        error: null,
        products
      };
    case getProductsByCategoryIdFailureAction.type:
      return {
        ...state,
        loading: false,
        error: action.error
      };
    case editProductInBasketSuccessAction.type:
      const { item } = action;

      return {
        ...state,
        edited: {
          ...item
        }
      };
    case editModeProductSuccessAction.type:
      const { product } = action;

      return {
        ...state,
        edited: {
          ...initCategoryProductState.edited,
          product
        }
      };
    case addCharmToProductSuccessAction.type:
      const charm = {
        charm: action.charm,
        sequence: state.edited.charms.length,
        forFree: state.edited.charms.length === 0 || !state.edited.charms.some(c => c.forFree)
      };
      const charms = [...state.edited.charms, charm];

      return {
        ...state,
        edited: {
          ...state.edited,
          charms
        }
      };
    case removeCharmFromProductAction.type:
      const newCharms = state.edited.charms.filter(c => c.charm.id !== action.id);
      const hasFreeCharm = newCharms.map((item2, index) => ({
        ...item2,
        forFree: index === 0 && !newCharms.some(c => c.forFree)
      }));

      return {
        ...state,
        edited: {
          ...state.edited,
          charms: hasFreeCharm
        }
      };
    case sortCharmInProductAction.type:
      const newArray = moveArrayItem(state.edited.charms, action.oldDraggableIndex, action.newDraggableIndex);
      const newSequence = newArray.map((item, index) => ({
        ...item,
        sequence: index + 1
      }));

      return {
        ...state,
        edited: {
          ...state.edited,
          charms: newSequence
        }
      };
    case updateDetailsAction.type:
      const { amount, comment } = action;

      return {
        ...state,
        edited: {
          ...state.edited,
          amount,
          comment
        }
      };
    case addProductToBasketSuccessAction.type:
      return {
        ...state,
        edited: {
          ...initCategoryProductState.edited
        }
      };
    default:
      return state;
  }
}
