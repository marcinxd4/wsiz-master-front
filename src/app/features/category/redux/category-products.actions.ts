import { createAction, props } from '@ngrx/store';
import { IProduct, IProductCharm } from '../../../core/interface/redux.interface';

export const getProductsByCategoryIdAction = createAction(
  '[Products by category] Get products by category id',
  props<{ category: string }>()
);
export const getProductsByCategoryIdSuccessAction = createAction(
  '[Products by category] Get products by category id success',
  props<{ products: any }>()
);
export const getProductsByCategoryIdFailureAction = createAction(
  '[Products by category] Get products by category id failure',
  props<{ error: string }>()
);
export const addCharmToEditedProduct = createAction(
  '[EditProduct] add charm to product',
  props<{ charm: IProductCharm }>()
);
export const sortCharmsSequence = createAction(
  '[EditProduct] sort charms sequence',
  props<{ charms: IProductCharm }>()
);
export const editModeProductAction = createAction(
  '[EditProduct] assign product to edited mode',
  props<{ productId: any }>()
);
export const editModeProductSuccessAction = createAction(
  '[EditProduct] assign product to edited mode success',
  props<{ product: IProduct }>()
);
export const editModeProductFailureAction = createAction(
  '[EditProduct] assign product to edited mode failure',
  props<{ error: string }>()
);
export const addProductToBasket = createAction('[Product] Add Product to basket');
export const resetEditedProduct = createAction('[Product] reset edited product');
export const setLoadingAction = createAction('[Category] set loading', props<{ loading: boolean }>());
