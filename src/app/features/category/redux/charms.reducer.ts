import { ICharmsState } from '../../../core/interface/redux.interface';
import { getCharmsAction, getCharmsFailureAction, getCharmsSuccessAction } from './charms.actions';

export const initCharmsState: ICharmsState = {
  charms: [],
  error: null,
  loading: false
};

export function charmsReducer(state = initCharmsState, action): ICharmsState {
  switch (action.type) {
    case getCharmsAction.type:
      return {
        ...state,
        loading: true
      };
    case getCharmsSuccessAction.type:
      const { charms } = action;

      return {
        ...state,
        error: null,
        loading: false,
        charms
      };
    case getCharmsFailureAction.type:
      return {
        ...state,
        error: action.error,
        loading: false
      };
    default:
      return { ...state };
  }
}
