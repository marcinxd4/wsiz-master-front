import { IState } from '../../../core/interface/redux.interface';
import { createSelector } from '@ngrx/store';

export const categoryFeature = (status: IState) => status.categories;

export const getCategoryProductsSelector = createSelector(
    categoryFeature,
    details => details.categories
);

export const loadingSelector = createSelector(
    categoryFeature,
    details => details.loading
);

export const errorSelector = createSelector(
    categoryFeature,
    details => details.error
);
