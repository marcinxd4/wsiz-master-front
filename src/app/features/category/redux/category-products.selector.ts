import { createSelector } from '@ngrx/store';
import { IState } from '../../../core/interface/redux.interface';

export const categoryProductsFeature = (state: IState) => state.products;

export const productsSelector = createSelector(
  categoryProductsFeature,
  details => details.products
);

export const productsErrorSelector = createSelector(
  categoryProductsFeature,
  details => details.error
);

export const productsLoadingSelector = createSelector(
  categoryProductsFeature,
  details => details.loading
);

export const getEditedProductData = createSelector(
  categoryProductsFeature,
  details => details.edited
);

export const editedProductSelector = createSelector(
  getEditedProductData,
  details => details.product
);

export const editedCharmsSelector = createSelector(
  getEditedProductData,
  details => details.charms
);

export const editedCountSelector = createSelector(
  getEditedProductData,
  details => details.amount
);

export const editedNoteSelector = createSelector(
  getEditedProductData,
  details => details.comment
);
