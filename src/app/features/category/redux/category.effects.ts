import { Injectable } from '@angular/core';
import {
  distinctUntilChanged,
  flatMap,
  map,
  catchError,
  tap,
  switchMap,
  take,
  withLatestFrom,
  filter
} from 'rxjs/operators';
import { Actions, ofType, createEffect } from '@ngrx/effects';

import {
  getProductsByCategoryIdAction,
  getProductsByCategoryIdSuccessAction,
  getProductsByCategoryIdFailureAction,
  editModeProductAction,
  resetEditedProduct,
  editModeProductSuccessAction,
  editModeProductFailureAction
} from '../redux/category-products.actions';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { of } from 'rxjs';
import {
  getCharmsAction,
  getCharmsFailureAction,
  getCharmsSuccessAction,
  addCharmToProductFailureAction,
  addCharmToProductAction,
  addCharmToProductSuccessAction
} from './charms.actions';
import { ICharmCategory, IProduct, IState, ICharm, IProductBasketItem } from '../../../core/interface/redux.interface';
import { CHARMS_URL, PRODUCTS_BY_CATEGORY_LIST_URL } from '../../../core/api/api.constatns';
import { select, Store } from '@ngrx/store';
import { getEditedProductData, productsSelector } from './category-products.selector';
import { MatDialog } from '@angular/material';
import { NoticeDialogComponent } from 'src/app/shared/component/notice-dialog/notice-dialog.component';
import { Router } from '@angular/router';

@Injectable()
export class CategoryProductsEffects {
  getProductsByCategoryId$ = createEffect(() =>
    this.actions$.pipe(
      ofType(getProductsByCategoryIdAction.type),
      filter(({ category }) => category !== null),
      switchMap(({ category }) =>
        this.httpClient.get(`${PRODUCTS_BY_CATEGORY_LIST_URL}/${category}`).pipe(
          distinctUntilChanged(),
          map((products: IProduct[]) => getProductsByCategoryIdSuccessAction({ products })),
          catchError((error: HttpErrorResponse) => of(getProductsByCategoryIdFailureAction({ error: error.message })))
        )
      )
    )
  );

  getAllCharms$ = createEffect(() =>
    this.actions$.pipe(
      ofType(getCharmsAction.type),
      switchMap(() =>
        this.httpClient.get(CHARMS_URL).pipe(
          map((charms: ICharmCategory[]) => getCharmsSuccessAction({ charms })),
          catchError((error: HttpErrorResponse) => of(getCharmsFailureAction({ error: error.message })))
        )
      )
    )
  );

  addCharmToProduct$ = createEffect(() =>
    this.actions$.pipe(
      ofType(addCharmToProductAction.type),
      withLatestFrom(this.store.pipe(select(getEditedProductData))),
      flatMap(([action, editedData]: [{ type: string; charm: ICharm }, IProductBasketItem]) => {
        const { charm } = action;
        const charmExist = editedData.charms.findIndex((item: any) => item.charm.id === charm.id) !== -1;
        if (editedData.charms.length < 5 && !charmExist) {
          return of(addCharmToProductSuccessAction({ charm }));
        } else if (editedData.charms.length >= 5) {
          return of(
            addCharmToProductFailureAction({
              error: 'Nie można dodać więcej niż 5 zawieszek!'
            })
          );
        } else if (charmExist) {
          return of(
            addCharmToProductFailureAction({
              error: 'Zawieszka została dodana!'
            })
          );
        }

        return of(
          addCharmToProductFailureAction({
            error: 'Wystąpił błąd spróbuj jeszcze raz'
          })
        );
      })
    )
  );

  addCharmFailure$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(addCharmToProductFailureAction.type),
        tap((action: { error: string }) => {
          this.dialog.open(NoticeDialogComponent, {
            data: { error: action.error, title: 'Ostrzeżenie' }
          });
        })
      ),
    { dispatch: false }
  );

  editNewCharm$ = createEffect(() =>
    this.actions$.pipe(
      ofType(editModeProductAction.type),
      withLatestFrom(this.store.pipe(select(productsSelector))),
      flatMap(([action, products]) => {
        const { productId } = action;
        const productIndex = products.findIndex(prod => prod.id === productId);

        if (productIndex !== -1) {
          return of(editModeProductSuccessAction({ product: products[productIndex] }));
        }

        return of(
          editModeProductFailureAction({
            error: 'Nie można dodać produktu do koszyka! Spróbuj jeszcze raz!'
          })
        );
      })
    )
  );

  constructor(
    private actions$: Actions,
    private httpClient: HttpClient,
    private store: Store<IState>,
    private router: Router,
    private dialog: MatDialog
  ) {}
}
