import { Injectable } from '@angular/core';
import { Actions } from '@ngrx/effects';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class HomeEffects {

  // getProducts$ = createEffect(() =>
  //   this.actions$.pipe(
  //     ofType(GET_PRODUCTS_ACTION),
  //     flatMap(() => {
  //       return this.httpClient.get(PRODUCTS_URL).pipe(
  //         distinctUntilChanged(),
  //         map((response: any) => new GetProducsSuccessAction(response))
  //       );
  //     })
  //   )
  // );


  constructor(private actions$: Actions,
              private httpClient: HttpClient) {
  }
}
