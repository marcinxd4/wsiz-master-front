import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Store, State, select } from '@ngrx/store';
import { signInAction } from '../redux/login.actions';
import { Observable } from 'rxjs';
import { authFailureSelector } from '../redux/login.selectors';
import { LoginData } from '../../../shared/interfaces/login.interface';
import { IState } from '../../../core/interface/redux.interface';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public error$: Observable<string> = this.store.pipe(select(authFailureSelector));
  public loginForm: FormGroup;

  constructor(private fb: FormBuilder,
    private store: Store<IState>) { }

  public onsubmit(): void {
    if (this.loginForm.valid) {
      const data: LoginData = this.loginForm.value;

      this.store.dispatch(signInAction({ data }));
    }
  }

  get email(): any {
    return this.loginForm.get('email');
  }

  get password(): any {
    return this.loginForm.get('password');
  }

  ngOnInit() {
    this.loginForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(14)]],
      rememberMe: [false],
    })
  }

}
