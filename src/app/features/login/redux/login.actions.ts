import { LoginData, AuthData } from './../../../shared/interfaces/login.interface';
import { Action, createAction, props } from '@ngrx/store';

export const signInAction = createAction('[Login] login User Action', props<{ data: LoginData }>());
export const signInSuccessAction = createAction('[Login] login User Success Action', props<{ data: AuthData }>());
export const signInFailureAction = createAction('[Login] login User Failure Action', props<{ error: string }>());
export const signOutAction = createAction('[Logout] logout user');
