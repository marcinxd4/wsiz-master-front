import { createSelector } from '@ngrx/store';
import { IState } from 'src/app/core/interface/redux.interface';

export const authFeature = (state: IState) => state.authentication;

export const authDataSelector = createSelector(
  authFeature,
  details => details
);
export const authFailureSelector = createSelector(
  authFeature,
  details => details.error
);
export const isAuthenticatedSelector = createSelector(
  authFeature,
  (details) => details
);

export const badgeContentCount = createSelector(
  (state: IState) => state.basket,
  details => details.products.length
)