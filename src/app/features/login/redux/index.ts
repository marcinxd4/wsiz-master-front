import { AuthData } from '../../../shared/interfaces/login.interface';
import { signInSuccessAction, signInFailureAction, signOutAction } from './login.actions';
import { registerFailureAction } from '../../register/redux/register.actions';

// tslint:disable-next-line: no-null-keyword
export const authInitState: AuthData = {
  userId: null,
  email: null,
  token: null,
  expires: null,
  refreshToken: null,
  userType: null,
  rememberMe: false,
  error: null
};

export function authReducer(state = authInitState, action) {
  switch (action.type) {
    case signInSuccessAction.type:
      const { data } = action;

      return {
        ...state,
        ...data
      };
    case registerFailureAction.type:
    case signInFailureAction.type:
      return {
        ...state,
        error: action.error
      };
    case signOutAction.type:
      return {
        ...authInitState
      };
    default:
      return { ...state };
  }
}
