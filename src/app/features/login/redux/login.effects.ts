import { Injectable } from '@angular/core';
import {
  distinctUntilChanged,
  map,
  switchMap,
  tap,
  catchError,
} from 'rxjs/operators';
import { Actions, ofType, createEffect } from '@ngrx/effects';

import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { AuthData } from '../../../shared/interfaces/login.interface';
import { Router, ActivatedRoute } from '@angular/router';
import { signInAction, signInSuccessAction, signInFailureAction, signOutAction } from './login.actions';
import { of } from 'rxjs';

@Injectable()
export class LoginEffects {
  loginUser$ = createEffect(() => this.actions$.pipe(
    ofType(signInAction.type),
    switchMap((action: any) => {
      const {data} = action;

      return this.httpClient.post('api/auth', data)
        .pipe(
          map((data: AuthData) => signInSuccessAction({data})),
          catchError((error: HttpErrorResponse) => of(signInFailureAction({error: error.message})))
        );
    })
  ));

  loginSuccess$ = createEffect(() => this.actions$.pipe(
    ofType(signInSuccessAction.type),
    distinctUntilChanged(),
    tap(data => {
      const returnUrl = this.activatedRoute.snapshot.queryParamMap.get('returnUrl');
      this.router.navigate([returnUrl ? returnUrl : '/']);
    })
  ), {dispatch: false});

  logoutAction$ = createEffect(() => this.actions$.pipe(
    ofType(signOutAction),
    tap(() => this.router.navigate(['/']))
  ), {dispatch: false});

  constructor(
    private actions$: Actions,
    private httpClient: HttpClient,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {
  }
}
