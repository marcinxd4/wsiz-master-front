import { Action } from '@ngrx/store';


export interface LoginFormDataAction extends Action {
  payload: any;
}

export interface AuthErrorActionData extends Action {
  payload: any;
}

export interface AuthActionData extends Action {
  payload: any;
}
