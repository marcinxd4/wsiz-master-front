import { MatchPasswordValidator } from './../match-password.validator';
import { Store } from '@ngrx/store';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { IState } from 'src/app/core/interface/redux.interface';
import { registerAction } from '../redux/register.actions';
import { regulationsError } from 'src/app/shared/directives/error-msg.constants';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.scss']
})
export class RegisterFormComponent implements OnInit {
  public registerForm: FormGroup;
  public regulationsErrorMsg = regulationsError;
  constructor(private fb: FormBuilder, private store: Store<IState>, private router: Router) {}

  public submitForm(): void {
    if (this.registerForm.valid) {
      const data = this.registerForm.value;
      this.store.dispatch(registerAction({ data }));
    }
  }

  public goToLogin(): void {
    this.router.navigate(['/', 'login']);
  }

  ngOnInit(): void {
    this.setUpRegisterForm();
  } 

  private setUpRegisterForm(): void {
    this.registerForm = this.fb.group(
      {
        email: ['', [Validators.required, Validators.email]],
        firstName: ['', Validators.required],
        lastName: ['', Validators.required],
        password: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(14)]],
        confirmPassword: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(14)]],
        acceptRegulations: [null, Validators.required]
      },
      {
        validator: MatchPasswordValidator.MatchPassword
      }
    );
  }
}
