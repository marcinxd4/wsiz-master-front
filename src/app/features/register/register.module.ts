import { MaterialModule } from 'src/app/core/material.module';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { RegisterComponent } from './register/register.component';
import { SharedModule } from '../../shared/shared.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { EffectsModule } from '@ngrx/effects';
import { RegisterFormComponent } from './register-form/register-form.component';
import { RegistrationService } from './services/registration.service';
import { RegisterEffects } from './redux/register.effects';
import { ErrorMsgDirective } from 'src/app/shared/directives/error-msg.directive';

const routes: Routes = [{ path: '', component: RegisterComponent }];

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(routes),
    FormsModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    EffectsModule.forRoot([RegisterEffects])
  ],
  exports: [RouterModule],
  providers: [RegistrationService],
  declarations: [RegisterComponent, RegisterFormComponent]
})
export class RegisterModule {}
