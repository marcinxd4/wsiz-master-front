import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Store, State, select } from '@ngrx/store';
import { ViewChild } from '@angular/core';
import { MatStepper } from '@angular/material/stepper';
import { RegistrationService } from '../services/registration.service';
import { IState } from 'src/app/core/interface/redux.interface';
import { Observable, Subscription } from 'rxjs';
import { authFeature, authFailureSelector } from '../../login/redux/login.selectors';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit, OnDestroy {
  @ViewChild('registerationStepper', { static: true }) private registrationStepper: MatStepper;
  public error$: Observable<string> = this.store.pipe(select(authFailureSelector));
  public isLinear = false;
  public registrationForm: FormGroup;
  public addressForm: FormGroup;

  private subscription: Subscription;
  constructor(
    private fb: FormBuilder,
    private registrationService: RegistrationService,
    private store: Store<IState>
  ) {}

  ngOnInit(): void {
    this.registrationProcessTracking();
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  private registrationProcessTracking(): void {
    this.subscription = this.registrationService.registrationObservable().subscribe(val => {
      if (val) {
        this.registrationStepper.next();
      }
    });
  }
}
