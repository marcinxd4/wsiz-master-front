import { AbstractControl } from '@angular/forms';
import { ValidateFn } from 'codelyzer/walkerFactory/walkerFn';

export class MatchPasswordValidator {
  static MatchPassword(control: AbstractControl) {
    const password = control.get('password').value;
    const confirmPassword = control.get('confirmPassword').value;

    if (password !== confirmPassword) {
      control.get('confirmPassword').setErrors({ NoPasswordMatch: true });
    }
  }
}

export class MatchNewPasswordValidator {
  static MatchPassword(control: AbstractControl) {
    const password = control.get('newPassword').value;
    const confirmPassword = control.get('confirmNewPassword').value;

    if (password !== confirmPassword) {
      control.get('confirmNewPassword').setErrors({ NoPasswordMatch: true });
    }
  }
}
