import { TestBed } from '@angular/core/testing';

import { RegistrationService } from './registration.service';
import { CommonModule } from '@angular/common';

describe('RegistrationService', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [CommonModule],
      providers: [RegistrationService]
    })
  );

  it('should be created', () => {
    const service: RegistrationService = TestBed.get(RegistrationService);
    expect(service).toBeTruthy();
  });
});
