import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { filter } from 'rxjs/operators';

@Injectable()
export class RegistrationService {
    private registrationStatus: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(null);

    public registrationObservable(): Observable<boolean> {
        return this.registrationStatus.pipe(
            filter(value => !!value)
        );
    }

    // tslint:disable-next-line:no-null-keyword
    public setRegistrationStatus(status: boolean = null): void {
        this.registrationStatus.next(status);
    }
}
