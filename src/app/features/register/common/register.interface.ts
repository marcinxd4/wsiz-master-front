import { Action } from "@ngrx/store";

export interface RegisterData {
    email: string;
    firstName: string;
    lastName: string;
    password: string;
    confirmPassword: string;
    phoneNumber: number;
}

export interface RegisterActionData extends Action {
    payload: RegisterData;
}