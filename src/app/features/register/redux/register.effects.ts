import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { omit } from 'lodash';
import { Injectable } from '@angular/core';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { map, tap, switchMap, catchError } from 'rxjs/operators';

import { RegistrationService } from '../services/registration.service';
import { of } from 'rxjs';
import { registerAction, registerSuccessAction, registerFailureAction } from './register.actions';
import { Router } from '@angular/router';

@Injectable()
export class RegisterEffects {
  register$ = createEffect(() =>
    this.actions$.pipe(
      ofType(registerAction.type),
      switchMap(({ data }) => {
        const body = omit(data, ['aggreement', 'phoneNumber']);

        return this.http.post('api/users', body).pipe(
          map((resp: any) => registerSuccessAction({ data: resp })),
          catchError((error: HttpErrorResponse) => of(registerFailureAction({ error: error.error.message })))
        );
      })
    )
  );

  registerSuccessful$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(registerSuccessAction.type),
        tap(data => {
          this.registrationService.setRegistrationStatus(data);
          this.router.navigate(['/login']);
        })
      ),
    { dispatch: false }
  );

  constructor(
    private http: HttpClient,
    private actions$: Actions,
    private registrationService: RegistrationService,
    private router: Router
  ) {}
}
