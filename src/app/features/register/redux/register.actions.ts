import { RegisterData } from '../common/register.interface';
import { createAction, props } from '@ngrx/store';

export const registerAction = createAction('[Registration] Registr new user', props<{ data: RegisterData }>());
export const registerSuccessAction = createAction('[Registration] Registr new user success', props<{ data: any }>());
export const registerFailureAction = createAction('[Registration] Registr new user failure', props<{ error: string }>());
export const registerLackOfAggrements = createAction('[Registration] Lack of aggremements');