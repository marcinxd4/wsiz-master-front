import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PASSWORD_RESET_URL } from 'src/app/core/api/api.constatns';
import { Observable } from 'rxjs';
import { ResetPasswordModel } from 'src/app/shared/interfaces/reset-password.model';

@Injectable({
  providedIn: 'root'
})
export class PasswordService {
  constructor(private httpClient: HttpClient) {}

  public sendEmailToRecovery(email): Observable<any> {
    const url = PASSWORD_RESET_URL + '/' + email;

    return this.httpClient.get(url);
  }

  public resetPassword(data: ResetPasswordModel): Observable<any> {
    const body = {
      ...data,
      token: ''
    };

    return this.httpClient.post(PASSWORD_RESET_URL, body);
  }
}
