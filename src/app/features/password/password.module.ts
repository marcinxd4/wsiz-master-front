import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';

import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MaterialModule } from 'src/app/core/material.module';
import { FirstStepComponent } from './first-step/first-step.component';
import { SecondStepComponent } from './second-step/second-step.component';

const routes: Routes = [
  { path: '', component: FirstStepComponent },
  { path: 'recovery', component: SecondStepComponent }
];

@NgModule({
  imports: [SharedModule, RouterModule.forChild(routes), FormsModule, MaterialModule, ReactiveFormsModule],
  declarations: [FirstStepComponent, SecondStepComponent]
})
export class PasswordModule {}
