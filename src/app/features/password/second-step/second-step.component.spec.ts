import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SecondStepComponent } from './second-step.component';
import { MaterialModule } from 'src/app/core/material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { ErrorMsgDirective } from 'src/app/shared/directives/error-msg.directive';
import { HttpClientModule } from '@angular/common/http';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { PasswordService } from '../service/password.service';

xdescribe('SecondStepComponent', () => {
  let component: SecondStepComponent;
  let fixture: ComponentFixture<SecondStepComponent>;
  let passwordServiceMock: PasswordService;
  beforeEach(async(() => {
    // tslint:disable-next-line: no-floating-promises
    TestBed.configureTestingModule({
      declarations: [SecondStepComponent, ErrorMsgDirective],
      imports: [MaterialModule, ReactiveFormsModule, HttpClientModule, NoopAnimationsModule],
      providers: [
        {
          provide: PasswordService,
          useValue: jasmine.createSpyObj('PasswordService', ['sendEmailToRecovery', 'resetPassword'])
        }
      ]
      // tslint:disable-next-line: newline-per-chained-call
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SecondStepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    passwordServiceMock = TestBed.get('PasswordService');
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  // describe('resetPassword', () => {
  //   it('should  not invoke anything', () => {});
  // });
});
