import { Component, OnInit } from '@angular/core';
import { PasswordService } from '../service/password.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { passwordValidMsg } from 'src/app/shared/directives/error-msg.constants';
import { MatchNewPasswordValidator } from '../../register/match-password.validator';

@Component({
  selector: 'app-second-step',
  templateUrl: './second-step.component.html',
  styleUrls: ['./second-step.component.scss']
})
export class SecondStepComponent implements OnInit {
  public resetPasswordForm: FormGroup;
  public passwordErrMsgConf = passwordValidMsg;
  constructor(private passwordService: PasswordService, private fb: FormBuilder) {}

  public resetPassword(): void {
    if (this.resetPasswordForm.valid) {
      this.passwordService.resetPassword(this.resetPasswordForm.value).subscribe(v => {
        console.log(v);
      });
    }
  }

  ngOnInit(): void {
    this.resetPasswordForm = this.fb.group(
      {
        email: ['', [Validators.email, Validators.required]],
        newPassword: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(14)]],
        confirmNewPassword: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(14)]]
      },
      {
        validator: MatchNewPasswordValidator.MatchPassword
      }
    );
  }
}
