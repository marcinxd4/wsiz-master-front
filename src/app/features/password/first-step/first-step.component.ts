import { Component } from '@angular/core';
import { PasswordService } from '../service/password.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-first-step',
  templateUrl: './first-step.component.html',
  styleUrls: ['./first-step.component.scss']
})
export class FirstStepComponent {
  public recoveryForm: FormGroup;
  constructor(private passwordService: PasswordService, private matSnackBar: MatSnackBar, private router: Router) {
    this.recoveryForm = new FormGroup({
      recoveryEmail: new FormControl('', [Validators.email, Validators.required])
    });
  }

  public sendEmailToPasswordRecovery(): void {
    const email = this.recoveryForm.get('recoveryEmail').value;
    this.passwordService
      .sendEmailToRecovery(email)
      .pipe(
        catchError(error => {
          this.matSnackBar.open('Użytkownik o takim adresie email nie istnieje', 'Błąd', {
            duration: 5000
          });

          return of(null);
        })
      )
      .subscribe(v => {
        if (v !== null) {
          const ref = this.matSnackBar.open('Na podany adres został wysłany email z dalszymi instrukcjami', 'Sukces', {
            duration: 5000
          });

          ref.afterDismissed().subscribe(c => {
            this.router.navigate(['/']);
          });
        }
      });
  }
}
