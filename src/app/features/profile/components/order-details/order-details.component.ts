import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { IState } from 'src/app/core/interface/redux.interface';
import { ActivatedRoute } from '@angular/router';
import { getOrderDetailsByIdAction } from '../../redux/actions';
import { orderDetailsSelector, profileErrorSelector, profileLoadingSelector } from '../../redux/selector';

@Component({
  selector: 'app-order-details',
  templateUrl: './order-details.component.html',
  styleUrls: ['./order-details.component.scss']
})
export class OrderDetailsComponent implements OnInit {
  public currentOrder$ = this.store.pipe(select(orderDetailsSelector));
  public loading$ = this.store.pipe(select(profileLoadingSelector));
  public error$ = this.store.pipe(select(profileErrorSelector));
  public orderId: string;
  constructor(private store: Store<IState>, private activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    const id = this.activatedRoute.snapshot.paramMap.get('id');
    this.store.dispatch(getOrderDetailsByIdAction({ id }));
    this.orderId = id;
  }
}
