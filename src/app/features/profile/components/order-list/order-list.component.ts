import { Component, OnInit, Input } from '@angular/core';
import { IState } from 'src/app/core/interface/redux.interface';
import { Store, select } from '@ngrx/store';
import { fetchMyOrdersAction } from '../../redux/actions';
import { profileLoadingSelector, profileErrorSelector, ordersListSelector } from '../../redux/selector';

@Component({
  selector: 'app-order-list',
  templateUrl: './order-list.component.html',
  styleUrls: ['./order-list.component.scss']
})
export class OrderListComponent implements OnInit {
  @Input() orders: any[];

  public loading$ = this.store.pipe(select(profileLoadingSelector));

  displayedColumns = ['id', 'isShipped', 'isPayed', 'basePrice', 'shipmentPrice', 'finalPrice', 'ACTIONS'];

  constructor(private store: Store<IState>) {}

  ngOnInit(): void {}
}
