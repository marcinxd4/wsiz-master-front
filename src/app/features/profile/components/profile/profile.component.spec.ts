import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileComponent } from './profile.component';
import { OrderListComponent } from '../order-list/order-list.component';
import { mockStore } from 'src/test/mockstore';
import { MaterialModule } from 'src/app/core/material.module';
import { CommonModule, APP_BASE_HREF } from '@angular/common';
import { RouterModule } from '@angular/router';
import { MockStore } from '@ngrx/store/testing';
import { IState } from 'src/app/core/interface/redux.interface';
import { Store } from '@ngrx/store';
import { fetchMyOrdersAction } from '../../redux/actions';
import { act } from '@ngrx/effects';

describe('ProfileComponent', () => {
  let component: ProfileComponent;
  let fixture: ComponentFixture<ProfileComponent>;
  let store: MockStore<IState>;
  beforeEach(async(() => {
    // tslint:disable-next-line: no-floating-promises
    TestBed.configureTestingModule({
      declarations: [ProfileComponent, OrderListComponent],
      imports: [MaterialModule, CommonModule, RouterModule.forRoot([])],
      providers: [mockStore, { provide: APP_BASE_HREF, useValue: '/' }]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    store = TestBed.get(Store);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should dispatch fetchMyOrdersAction', () => {
    const dispatchSpy = spyOn(store, 'dispatch');
    const action = fetchMyOrdersAction();

    component.ngOnInit();

    expect(dispatchSpy).toHaveBeenCalledWith(action);
  });
});
