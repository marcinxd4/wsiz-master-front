import { Component, OnInit } from '@angular/core';
import { profileErrorSelector, profileLoadingSelector, ordersListSelector } from '../../redux/selector';
import { fetchMyOrdersAction } from '../../redux/actions';
import { IState } from 'src/app/core/interface/redux.interface';
import { Store, select } from '@ngrx/store';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  public error$ = this.store.pipe(select(profileErrorSelector));
  public loading$ = this.store.pipe(select(profileLoadingSelector));
  public orders$ = this.store.pipe(select(ordersListSelector));

  constructor(private store: Store<IState>) {}

  ngOnInit(): void {
    this.store.dispatch(fetchMyOrdersAction());
  }
}
