import { NgModule } from "@angular/core";
import { AddressComponent } from "./components/address/address.component";
import { RouterModule, Routes } from "@angular/router";
import { ProfileComponent } from "./components/profile/profile.component";
import { SharedModule } from "../../shared/shared.module";
import { OrderDetailsComponent } from "./components/order-details/order-details.component";
import { OrderListComponent } from "./components/order-list/order-list.component";
import { AuthGuard } from "src/app/core/guars/auth.guard";
import { HttpClientModule } from "@angular/common/http";
import { CommonModule } from "@angular/common";
import { MaterialModule } from "src/app/core/material.module";

const routes: Routes = [
  { path: "", component: ProfileComponent },
  { path: "address", component: AddressComponent, canActivate: [AuthGuard] },
  {
    path: "my-orders/:id",
    component: OrderDetailsComponent,
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [SharedModule, RouterModule.forChild(routes), CommonModule, MaterialModule],
  declarations: [
    AddressComponent,
    ProfileComponent,
    OrderListComponent,
    OrderDetailsComponent
  ]
})
export class ProfileModule {}
