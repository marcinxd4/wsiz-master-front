import { createAction, props } from "@ngrx/store";

export const GET_PROFILE_DATA = '[Profile] Get profile data';
export const ADD_USER_ADDRESS = '[Profile] Add user address';

export const getProfileData = createAction('[Profile] get profile data', props<{data: any}>());
export const getUserAddressData = createAction('[Profile] get user address data', props<{data: any}>());