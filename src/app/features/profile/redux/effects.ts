import { createEffect, Actions, ofType } from '@ngrx/effects';
import { Injectable } from '@angular/core';
import {
  getOrderDetailsByIdAction,
  getOrderDetailsByIdSuccessAction,
  getOrderDetailsByIdFailureAction,
  fetchMyOrdersAction,
  fetchMyOrdersSuccessAction
} from './actions';
import { ORDERS_URL } from 'src/app/core/api/api.constatns';
import { switchMap, map, catchError } from 'rxjs/operators';
import { HttpErrorResponse, HttpClient } from '@angular/common/http';
import { of } from 'rxjs';
import { IOrderItem } from 'src/app/shared/interfaces/common.interface';
import { MY_ORDER_LIST } from './state';

@Injectable()
export class ProfileEffect {
  fetchOrderById$ = createEffect(() =>
    this.actions$.pipe(
      ofType(getOrderDetailsByIdAction.type),
      switchMap(action => {
        const { id } = action;

        return this.httpClient.get<IOrderItem>(ORDERS_URL, { params: { id } }).pipe(
          map((order: IOrderItem) => getOrderDetailsByIdSuccessAction({ order })),
          catchError((error: HttpErrorResponse) => {
            const message = error.status === 403 ? 'Nie możesz przeglądać nie swoich zamównien' : error.message;
            return of(getOrderDetailsByIdFailureAction({ error: message }));
          })
        );
      })
    )
  );

  fetchOrders$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fetchMyOrdersAction.type),
      switchMap(() =>
        this.httpClient.get(ORDERS_URL).pipe(
          map(response =>
            fetchMyOrdersSuccessAction({ orders: response }))
        )
      )
    )
  );

  getOrderById$ = createEffect(() => this.actions$.pipe(ofType()), { dispatch: false });
  constructor(private actions$: Actions, private httpClient: HttpClient) {}
}
