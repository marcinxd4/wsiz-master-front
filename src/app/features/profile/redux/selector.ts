import { IState } from "src/app/core/interface/redux.interface";
import { createSelector } from "@ngrx/store";

export const profileFeature = (state: IState) => state.profile;

export const orderDetailsSelector = createSelector(
    profileFeature,
    details => details.orderDetail
);

export const ordersListSelector =  createSelector(
    profileFeature,
    details => details.orders
);

export const profileLoadingSelector =  createSelector(
    profileFeature,
    details => details.loading
);

export const profileErrorSelector =  createSelector(
    profileFeature,
    details => details.error
);