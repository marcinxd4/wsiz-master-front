import { createAction, props } from "@ngrx/store";
import { IOrderItem } from "src/app/shared/interfaces/common.interface";

export const fetchMyOrdersAction = createAction(
    "[Basket Orders] Fetch my orders"
  );
  export const fetchMyOrdersSuccessAction = createAction(
    "[Basket Orders] Fetch my orders success",
    props<{ orders: any }>()
  );
  export const fetchMyOrdersFailureAction = createAction(
    "[Basket Orders] Fetch my orders error",
    props<{ error: string }>()
  );
  
  export const getOrderDetailsByIdAction = createAction(
    "[Orders] get order by id",
    props<{ id: number | string }>()
  );
  export const getOrderDetailsByIdSuccessAction = createAction(
    "[Orders] get order by id success action",
    props<{ order: IOrderItem }>()
  );
  export const getOrderDetailsByIdFailureAction = createAction(
    "[Orders] get order by id failure action",
    props<{ error: string }>()
  );
  