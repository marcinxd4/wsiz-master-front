import {
  getOrderDetailsByIdSuccessAction,
  getOrderDetailsByIdAction,
  getOrderDetailsByIdFailureAction,
  fetchMyOrdersSuccessAction,
  fetchMyOrdersAction,
  fetchMyOrdersFailureAction
} from './actions';
import { initProfileState, IProfileState } from './state';

export function profileReducer(state: IProfileState = initProfileState, action): IProfileState {
  switch (action.type) {
    case getOrderDetailsByIdAction.type:
      return {
        ...state,
        error: null,
        loading: true
      };
    case getOrderDetailsByIdSuccessAction.type:
      const { order } = action;
      return {
        ...state,
        loading: false,
        error: null,
        orderDetail: order
      };
    case getOrderDetailsByIdFailureAction.type:
      return {
        ...state,
        loading: false,
        error: action.error
      };

    case fetchMyOrdersAction.type:
      return {
        ...state,
        loading: true,
        error: null
      };
    case fetchMyOrdersSuccessAction.type:
      return {
        ...state,
        loading: false,
        error: null,
        orders: action.orders
      };
    case fetchMyOrdersFailureAction.type:
      return {
        ...state,
        loading: false,
        error: action.error
      };
    default:
      return state;
  }
}
