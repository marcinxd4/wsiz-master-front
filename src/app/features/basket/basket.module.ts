import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { MaterialModule } from '../../core/material.module';

import { BasketComponent } from './components/basket/basket.component';
import { OrderComponent } from './components/order/order.component';
import { ProductPricePipe } from './product-pipe/product-price.pipe';
import { ProductPriceFullPipe } from './product-pipe/product-price-full.pipe';
// tslint:disable-next-line: no-implicit-dependencies
import { AuthGuard } from 'src/app/core/guars/auth.guard';
import { SharedModule } from 'src/app/shared/shared.module';
import { OrderTableComponent } from './components/order-table/order-table.component';
import { BasketEmptyGuard } from 'src/app/core/guars/basket-empty.guard';

const routes: Routes = [
  { path: '', component: BasketComponent },
  { path: 'order', component: OrderComponent, canActivate: [AuthGuard, BasketEmptyGuard] }
];

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule,
    RouterModule.forChild(routes)
  ],
  declarations: [BasketComponent, OrderComponent, ProductPricePipe, ProductPriceFullPipe, OrderTableComponent],
  exports: [ProductPricePipe, ProductPriceFullPipe]
})
export class BasketModule {}
