import { IProductBasketItem } from '../../../core/interface/redux.interface';
import { IAddress } from '../../../core/interface/order.interface';
import { IPromoCode, IPromoCodeStatus } from 'src/app/core/interface/promo-code.interface';

export interface IBasketState {
  products: IProductBasketItem[];
  shipmentId: string | number;
  address: IAddress;
  comment: string;
  promoCode: string;
  error: string;
  loading: boolean;
  addressId: string | null;
  promoCodeStatus: IPromoCodeStatus;
}

export const basketInitState: IBasketState = {
  products: [],
  shipmentId: null,
  address: null,
  comment: null,
  promoCode: null,
  error: null,
  addressId: null,
  loading: false,
  promoCodeStatus: {
    promoCode: null,
    isPromoCodeActive: false,
    error: null,
    loading: false
  }
};
