import { createAction, props } from '@ngrx/store';
import { IProductBasketItem } from '../../../core/interface/redux.interface';
import { IShipment } from '../../../core/interface/shipment.interface';
import { IBasketState } from './state';
import { IPromoCode } from 'src/app/core/interface/promo-code.interface';

export const addProductToBasketAction = createAction('[Basket] Add product to basket');
export const addProductToBasketSuccessAction = createAction(
  '[Basket] Add product to basket Success',
  props<{ product: any }>()
);
export const addProductToBasketFailureAction = createAction(
  '[Basket] Add product to basket Failure',
  props<{ error: string }>()
);

export const selectAddressIdAction = createAction(
  '[SelectAddress] Select addresss id',
  props<{ addressId: string | null }>()
);

export const removeProductFromBasketAction = createAction(
  '[Basket] Remove product from basket',
  props<{ indexProduct: number }>()
);

export const updateProductInBasketAction = createAction('[Basket] Update product basket', props<{ product_id: any }>());
export const updateProductInBasketSuccessAction = createAction(
  '[Basket] Update product basket success',
  props<{ product_id: any }>()
);
export const updateProductInBasketFailureAction = createAction(
  '[Basket] Update product basket failure',
  props<{ error: string }>()
);
export const orderProductFromBasketAction = createAction(
  '[Basket] Order product from basket',
  props<{ product_id: any }>()
);
export const orderProductFromBasketSuccessAction = createAction(
  '[Basket] Order product from basket success',
  props<{ product_id: any }>()
);
export const orderProductFromBasketFailureAction = createAction(
  '[Basket] Order product from basket failure',
  props<{ product_id: any }>()
);

export const orderProductsAction = createAction('[order] order products');
export const orderProductsSuccessAction = createAction('[order] order product success', props<{ order: any }>());
export const orderProductsFailureAction = createAction('[order] order product failure', props<{ error: any }>());

export const updateShipmentAction = createAction('[oder] update shipment action', props<{ shipment: IShipment }>());
export const updatePromoCodeAction = createAction('[oder] update promoCode action', props<{ promoCode: string }>());
export const updateCommentAction = createAction('[oder] update promo code action', props<{ comment: string }>());
export const updateAddressAction = createAction('[oder] update address action', props<{ address: any }>());

export const resetBsketAction = createAction('[basket] reset basket action');

export const editProductInBasketAction = createAction(
  '[BasketEdit] edit produuct in basket',
  props<{ index: number }>()
);
export const editProductInBasketSuccessAction = createAction(
  '[BasketEdit] edit produuct in basket success',
  props<{ item: IProductBasketItem }>()
);
export const editProductInBasketFailureAction = createAction(
  '[BasketEdit] edit produuct in basket failure',
  props<{ error: string }>()
);

export const saveEditedProductToBasketAction = createAction('[SaveEditedBasketProduct ] Save edited basket product');
export const saveEditedProductToBasketSuccessAction = createAction(
  '[SaveEditedBasketProduct ] Save edited basket success product',
  props<{ item: IProductBasketItem; index: number }>()
);
export const saveEditedProductToBasketFailureAction = createAction(
  '[SaveEditedBasketProduct ] Save edited basket failure product',
  props<{ error: string }>()
);

export const saveUpdateProductInBasketAction = createAction('[UpdateProductInBasket] save updated product in basket ');
export const checkPromoCodeAction = createAction(
  '[UpdateProductInBasket] checkPromoCode action ',
  props<{ code: string }>()
);
export const checkPromoCodeSuccessAction = createAction(
  '[UpdateProductInBasket] checkPromoCode success action',
  props<{ response: IPromoCode }>()
);
export const checkPromoCodeFailureAction = createAction(
  '[UpdateProductInBasket] checkPromoCode failure action ',
  props<{ error: string }>()
);
export const resetPromoCode = createAction('[basket] reset promoCode');