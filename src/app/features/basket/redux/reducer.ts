import { basketInitState, IBasketState } from './state';
import {
  addProductToBasketSuccessAction,
  removeProductFromBasketAction,
  resetBsketAction,
  selectAddressIdAction,
  updateAddressAction,
  updateCommentAction,
  updatePromoCodeAction,
  updateShipmentAction,
  saveEditedProductToBasketSuccessAction,
  checkPromoCodeAction,
  checkPromoCodeFailureAction,
  checkPromoCodeSuccessAction,
  resetPromoCode
} from './actions';

export function basketReducer(state: IBasketState = basketInitState, action: any): IBasketState {
  switch (action.type) {
    case addProductToBasketSuccessAction.type:
      const { product } = action;
      return {
        ...state,
        products: [...state.products, product]
      };
    case removeProductFromBasketAction.type:
      const { indexProduct } = action;

      return {
        ...state,
        products: state.products.filter((item, index) => index !== indexProduct)
      };
    case resetBsketAction.type:
      return {
        ...state,
        products: []
      };
    case updateAddressAction.type:
      return {
        ...state,
        address: action.address
      };
    case updateCommentAction.type:
      return {
        ...state,
        comment: action.comment
      };
    case updateShipmentAction.type:
      return {
        ...state,
        shipmentId: action.shipment
      };
    case updatePromoCodeAction.type:
      return {
        ...state,
        promoCode: action.promoCode
      };
    case selectAddressIdAction.type:
      return {
        ...state,
        addressId: action.addressId !== 'null' ? action.addressId : null,
        address: {
          ...basketInitState.address
        }
      };
    case saveEditedProductToBasketSuccessAction.type:
      const { item, index } = action;
      return {
        ...state,
        products: [...state.products.slice(0, index), item, ...state.products.slice(index + 1)]
      };
    case checkPromoCodeAction.type:
      return {
        ...state,
        promoCodeStatus: {
          ...state.promoCodeStatus,
          loading: true
        }
      };
    case checkPromoCodeSuccessAction.type:
      const promoCode = action.response.isActive ? action.response : null;

      return {
        ...state,
        promoCodeStatus: {
          ...state.promoCodeStatus,
          isPromoCodeActive: action.response.isActive,
          promoCode,
          loading: false
        }
      };
    case checkPromoCodeFailureAction.type:
      return {
        ...state,
        promoCodeStatus: {
          ...state.promoCodeStatus,
          isPromoCodeActive: false,
          promoCode: null,
          error: action.error,
          loading: false
        }
      };
    case resetPromoCode.type:
      return {
        ...state,
        promoCode: null,
        promoCodeStatus: {
          ...basketInitState.promoCodeStatus
        }
      };
    default:
      return { ...state };
  }
}
