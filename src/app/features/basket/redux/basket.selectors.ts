import { IState } from '../../../core/interface/redux.interface';
import { createSelector } from '@ngrx/store';

export const basketFeature = (state: IState) => state.basket;

export const basketProducts = createSelector(
  basketFeature,
  details => details.products
);

export const addressIdSelector = createSelector(
  basketFeature,
  details => details.addressId
);
export const promoCodeSelector = createSelector(
  basketFeature,
  details => details.promoCode
);
export const promoCodeStatus = createSelector(
  basketFeature,
  details => details.promoCodeStatus
);

export const promoCodeStatusLoading = createSelector(
  promoCodeStatus,
  details => details.loading
);

export const promoCodeStatusActive = createSelector(
  promoCodeSelector,
  promoCodeStatus,
  (promoCode, details) => details.isPromoCodeActive
);
export const promoCodeStatusError = createSelector(
  promoCodeSelector,
  promoCodeStatus,
  (promoCode, details) =>
    promoCode !== null && details.error !== null && details.promoCode === null
);
export const categoryFeature = (state: IState) => state.categories;
