import { Injectable } from '@angular/core';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { catchError, flatMap, map, switchMap, tap, withLatestFrom } from 'rxjs/operators';
import {
  addProductToBasketAction,
  addProductToBasketFailureAction,
  addProductToBasketSuccessAction,
  orderProductsAction,
  orderProductsFailureAction,
  orderProductsSuccessAction,
  editProductInBasketSuccessAction,
  editProductInBasketFailureAction,
  editProductInBasketAction,
  saveEditedProductToBasketAction,
  saveEditedProductToBasketSuccessAction,
  resetBsketAction,
  checkPromoCodeSuccessAction,
  checkPromoCodeFailureAction,
  checkPromoCodeAction
} from './actions';
import { of } from 'rxjs';
import { MatSnackBar, MatDialog } from '@angular/material';
import { IState, IProductBasketItem } from '../../../core/interface/redux.interface';
import { select, Store } from '@ngrx/store';
import { basketFeature, categoryFeature } from './basket.selectors';
import { ORDERS_URL, ORDER_DISCOUNT_URL } from '../../../core/api/api.constatns';
import { getEditedProductData } from '../../category/redux/category-products.selector';
import { IBasketState } from './state';
import { Router } from '@angular/router';
import { SuccessDialogComponent } from 'src/app/core/components/success-dialog/success-dialog.component';
import { ERROR_CODES } from '../basket.constants';
import { IPromoCode } from 'src/app/core/interface/promo-code.interface';

@Injectable()
export class BasketEffect {
  addToBasket$ = createEffect(() =>
    this.actions$.pipe(
      ofType(addProductToBasketAction.type),
      withLatestFrom(this.store.pipe(select(getEditedProductData))),
      flatMap(([action, product]) => {
        if (product.product.id !== null) {
          return of(addProductToBasketSuccessAction({ product }));
        }

        return of(
          addProductToBasketFailureAction({
            error: 'Cannot add product to basket'
          })
        );
      })
    )
  );

  addToBasketSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(addProductToBasketSuccessAction.type),
        withLatestFrom(this.store.pipe(select(categoryFeature))),
        tap(([action, categories]) => {
          const { product } = action;
          const categoryId = (product as IProductBasketItem).product.productCategoryId;
          let category = null;
          categories.categories.forEach(element => {
            element.productCategories.forEach(item => {
              if (item.id === categoryId) {
                category = item;
              }
            });
          });
          this.snackBar.open('Produkt dodany do koszyka', 'Success', {
            duration: 3000
          });
          this.router.navigate(['/', 'category', category.name], { queryParams: { categoryId } });
        })
      ),
    { dispatch: false }
  );

  orderProducts$ = createEffect(() =>
    this.actions$.pipe(
      ofType(orderProductsAction.type),
      withLatestFrom(this.store.pipe(select(basketFeature))),
      switchMap(([_, basket]) => {
        const address =
          basket.addressId !== 'null' ? { addressId: basket.addressId } : { address: { ...basket.address } };
        const newBasket = {
          ...address,
          comment: basket.comment,
          promoCode: basket.promoCode,
          shipmentId: basket.shipmentId,
          products: basket.products.map(({ amount, charms, product, comment }) => ({
            amount,
            comment,
            productId: product.id,
            charms: charms.map(({ sequence, charm }) => ({
              sequence,
              charmId: charm.id
            }))
          }))
        };

        return this.httpClient.post(ORDERS_URL, newBasket).pipe(
          map(item => orderProductsSuccessAction({ order: item })),
          catchError((error: HttpErrorResponse) => of(orderProductsFailureAction({ error: error.error })))
        );
      })
    )
  );

  orderProductsSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(orderProductsSuccessAction.type),
        tap(action => {
          const { order } = action;
          const dialogRef = this.dialog.open(SuccessDialogComponent, {
            data: {
              title: 'Udało się!',
              message: `Złożono zamówienie! Numer zamówienia to ${(order as { id: number }).id}.`
            }
          });
          dialogRef.afterClosed().subscribe(result => {
            this.router.navigate(['/', 'profile', 'my-orders', (order as { id: number }).id]);
          });
        })
      ),
    { dispatch: false }
  );

  orderFailure$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(orderProductsFailureAction.type),
        tap((action: { error: any }) => {
          let message: string;
          message = '';
          if (action.error.errors) {
            Object.keys(action.error.errors).forEach(key => {
              message += ERROR_CODES[key] + '\n';
            });
          } else {
            const { errorCodeName } = action.error;
            message += ERROR_CODES[errorCodeName] + '\n';
          }

          const dialogRef = this.dialog.open(SuccessDialogComponent, {
            data: {
              title: 'Błąd!',
              message,
              failure: true
            }
          });
        })
      ),
    { dispatch: false }
  );

  orderSuccessResetBasket$ = createEffect(() =>
    this.actions$.pipe(
      ofType(orderProductsSuccessAction.type),
      flatMap(() => of(resetBsketAction()))
    )
  );

  editProduct$ = createEffect(() =>
    this.actions$.pipe(
      ofType(editProductInBasketAction.type),
      withLatestFrom(this.store.pipe(select(basketFeature))),
      flatMap(([action, basket]: [{ index: number }, IBasketState]) => {
        const item = basket.products[action.index];
        if (item) {
          return of(
            editProductInBasketSuccessAction({
              item: { ...item, index: action.index }
            })
          );
        }

        return of(
          editProductInBasketFailureAction({
            error: 'Błąd! Nie można edytowac produktu.'
          })
        );
      })
    )
  );

  editSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(editProductInBasketSuccessAction.type),
        tap((action: { item: IProductBasketItem }) => {
          const { item } = action;
          // tslint:disable-next-line: whitespace
          this.router.navigate(['/', 'category', item.product.productCategoryId, 'product', item.product.id], {
            queryParams: { edit: 'edit' }
          });
        })
      ),
    { dispatch: false }
  );

  editProductFailure$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(editProductInBasketFailureAction.type),
        tap(({ error }) => {})
      ),
    {
      dispatch: false
    }
  );

  saveEditedBasketProduct$ = createEffect(() =>
    this.actions$.pipe(
      ofType(saveEditedProductToBasketAction.type),
      withLatestFrom(this.store.pipe(select(getEditedProductData))),
      flatMap(([action, product]) => {
        const item = {
          product: product.product,
          charms: product.charms,
          comment: product.comment,
          amount: product.amount
        };
        const index = product.index;

        return of(saveEditedProductToBasketSuccessAction({ item, index }));
      })
    )
  );

  updatedProductInBasket$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(saveEditedProductToBasketSuccessAction.type),
        withLatestFrom(this.store.pipe(select(categoryFeature))),
        tap(([action, categories]) => {
          const { item } = action;
          const categoryId = (item as any).product.productCategoryId;

          let category = null;
          categories.categories.forEach(element => {
            element.productCategories.forEach(item => {
              if (item.id === categoryId) {
                category = item;
              }
            });
          });

          this.snackBar.open('Produkt zaktualizowany', 'Success', {
            duration: 3000
          });
          this.router.navigate(['/', 'category', category.name], { queryParams: { categoryId } });
        })
      ),
    { dispatch: false }
  );

  checkPromoCode$ = createEffect(() =>
    this.actions$.pipe(
      ofType(checkPromoCodeAction.type),
      flatMap(({ code }) =>
        this.httpClient.get(ORDER_DISCOUNT_URL + '/' + code).pipe(
          map((response: IPromoCode) => {
            if (response.isActive) {
              return checkPromoCodeSuccessAction({ response });
            }
            return checkPromoCodeFailureAction({
              error: 'Kod promocyjny jest nieaktywny'
            });
          }),
          catchError((error: HttpErrorResponse) =>
            of(
              checkPromoCodeFailureAction({
                error: 'Kod promocyjny nie istnieje'
              })
            )
          )
        )
      )
    )
  );

  promoCodeFailure$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(checkPromoCodeFailureAction.type),
        tap(({ error }) => {
          this.snackBar.open(error, 'Błąd', {
            duration: 3000
          });
        })
      ),
    { dispatch: false }
  );

  promoCodeSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(checkPromoCodeSuccessAction.type),
        tap(({ error }) => {
          this.snackBar.open('Kod promocyjny aktywny', 'Success', {
            duration: 3000
          });
        })
      ),
    { dispatch: false }
  );

  constructor(
    private actions$: Actions,
    private httpClient: HttpClient,
    private snackBar: MatSnackBar,
    private dialog: MatDialog,
    private store: Store<IState>,
    private router: Router
  ) {}
}
