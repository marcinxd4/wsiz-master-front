import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { IProductBasketItem, IState } from '../../../../core/interface/redux.interface';
import { select, Store } from '@ngrx/store';
import { basketProducts } from '../../redux/basket.selectors';
import { MatDialog } from '@angular/material';
import { ConfigmDialogComponent } from '../../../../core/components/configm-dialog/configm-dialog.component';
import { CONFIRMATION_DIALOG } from '../../../../core/constats/common';
import { removeProductFromBasketAction, editProductInBasketAction } from '../../redux/actions';
import { ProductPreviewComponent } from '../../../../shared/components/product-preview/product-preview.component';

@Component({
  selector: 'app-order-table',
  templateUrl: './order-table.component.html',
  styleUrls: ['./order-table.component.scss']
})
export class OrderTableComponent {
  public readonly displayedColumns: string[] = ['name', 'comment', 'amount', 'charmsAmount', 'totalPrice', 'action'];
  public basketProducts$: Observable<IProductBasketItem[]> = this.store.pipe(select(basketProducts));

  constructor(private store: Store<IState>, public dialog: MatDialog) {}

  public removeProduct(index: number): void {
    const configmDialogRef = this.dialog.open(ConfigmDialogComponent, {
      data: { title: CONFIRMATION_DIALOG.TITLE, message: CONFIRMATION_DIALOG.MESSAGE }
    });

    configmDialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.store.dispatch(removeProductFromBasketAction({ indexProduct: index }));
      }
    });
  }

  public editProductInBasket(index: number): void {
    this.store.dispatch(editProductInBasketAction({ index }));
  }

  public openDisplay(product: any): void {
    const dialogRef = this.dialog.open(ProductPreviewComponent, {
      data: { product }
    });
  }
}
