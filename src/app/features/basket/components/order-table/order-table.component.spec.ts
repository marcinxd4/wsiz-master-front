import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderTableComponent } from './order-table.component';
import { MaterialModule } from 'src/app/core/material.module';
import { mockStore } from 'src/test/mockstore';
import { MatDialog } from '@angular/material';
import { ProductFullPricePipe } from 'src/app/shared/pipes/product-full-price.pipe';
import { ProductPriceFullPipe } from '../../product-pipe/product-price-full.pipe';
import { MockStore } from '@ngrx/store/testing';
import { IState } from 'src/app/core/interface/redux.interface';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';
import { removeProductFromBasketAction, editProductInBasketAction } from '../../redux/actions';
import { ProductPreviewComponent } from 'src/app/shared/components/product-preview/product-preview.component';
import { GalleryComponent } from 'src/app/shared/components/gallery/gallery.component';

describe('OrderTableComponent', () => {
  let component: OrderTableComponent;
  let fixture: ComponentFixture<OrderTableComponent>;
  let store: MockStore<IState>;
  const matDialogMock = { open: jasmine.createSpy('open') };

  beforeEach(async(() => {
    // tslint:disable-next-line: no-floating-promises
    TestBed.configureTestingModule({
      declarations: [
        OrderTableComponent,
        ProductFullPricePipe,
        ProductPreviewComponent,
        GalleryComponent,
        ProductPriceFullPipe
      ],
      imports: [MaterialModule],
      providers: [
        mockStore,
        ProductPriceFullPipe,
        ProductFullPricePipe,
        {
          provide: MatDialog,
          useValue: matDialogMock
        }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    store = TestBed.get(Store);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should dispatch editProductInBasketAction', () => {
    const action = editProductInBasketAction({ index: 1 });
    const dispatchSpy = spyOn(store, 'dispatch');
    component.editProductInBasket(1);

    expect(dispatchSpy).toHaveBeenCalledWith(action);
  });

  it('should invoke dialog open method with product preview component', () => {
    const product = { test: 'test' } as any;
    component.openDisplay(product);

    expect(matDialogMock.open).toHaveBeenCalledWith(ProductPreviewComponent, { data: { product } });
  });

  // it('should open dialog with confirmDialogComponent and dispatch removeProductFromBasketAction when result true', () => {
  //   const dispatchSpy = spyOn(store, 'dispatch');
  //   const action = removeProductFromBasketAction({ indexProduct: 1 });
  //   matDialogMock.open.and.returnValue({ afterClosed: () => of(1) });

  //   component.removeProduct(1);

  //   expect(matDialogMock.open).toHaveBeenCalled();

  //   setTimeout(() => {
  //     // tslint:disable-next-line: no-floating-promises
  //     expect(dispatchSpy).toHaveBeenCalledWith(action);
  //   }, 1500);
  // });
});
