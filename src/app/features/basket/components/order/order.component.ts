import { Component, OnInit, OnDestroy } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IState } from '../../../../core/interface/redux.interface';
import { getShipmentAction } from '../../../../core/actions/shipment.actions';
import { Observable, combineLatest, Subscription } from 'rxjs';
import { IShipment } from '../../../../core/interface/shipment.interface';
import { getShipments } from '../../../../core/selectors/shipments.selectors';
import { phoneNumberValidMsg, postCodeValidMsg } from '../../../../shared/directives/error-msg.constants';
import {
  orderProductsAction,
  selectAddressIdAction,
  updateAddressAction,
  updateCommentAction,
  updatePromoCodeAction,
  updateShipmentAction,
  checkPromoCodeAction,
  resetPromoCode
} from '../../redux/actions';
import { getUserAddressesAction } from '../../../../core/actions/address.actions';
import { IAddress } from '../../../../core/interface/order.interface';
import { addressesListSelector } from '../../../../core/selectors/address.selector';
import { IBasketState } from '../../redux/state';
import {
  addressIdSelector,
  basketFeature,
  promoCodeStatusActive,
  promoCodeStatusError,
  promoCodeStatusLoading,
  promoCodeStatus,
  promoCodeSelector,
  basketProducts
} from '../../redux/basket.selectors';
import { first, map } from 'rxjs/operators';
// tslint:disable-next-line: no-implicit-dependencies
import { IPromoCodeStatus } from 'src/app/core/interface/promo-code.interface';
import { ProductPriceFullPipe } from '../../product-pipe/product-price-full.pipe';
import { ProductFullPricePipe } from 'src/app/shared/pipes/product-full-price.pipe';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})
export class OrderComponent implements OnInit, OnDestroy {
  subscription = new Subscription();
  public shipmentPrice$: Observable<number>;
  orderAddressForm: FormGroup;
  orderShipmentForm: FormGroup;
  shipments$: Observable<IShipment[]> = this.store.pipe(select(getShipments));
  addressId$: Observable<string> = this.store.pipe(select(addressIdSelector));
  public addresses$: Observable<IAddress[]> = this.store.pipe(select(addressesListSelector));
  public basket$: Observable<IBasketState> = this.store.pipe(select(basketFeature));
  public promoCodeStatus$: Observable<IPromoCodeStatus> = this.store.pipe(select(promoCodeStatus));
  public promoCodeStatusLoading$: Observable<boolean> = this.store.pipe(select(promoCodeStatusLoading));
  public promoCodeStatusError$: Observable<string | boolean> = this.store.pipe(select(promoCodeStatusError));
  public promoCodeStatusActive$: Observable<boolean> = this.store.pipe(select(promoCodeStatusActive));
  public promocodeAddButton$: Observable<boolean> = combineLatest([
    this.promoCodeStatus$,
    this.store.pipe(select(promoCodeSelector))
  ]).pipe(map(([status, code]) => (status.promoCode === null || status.error === null) && !status.loading));
  
  public discountCodePrice$ = combineLatest([this.store.pipe(select(basketProducts)), this.promoCodeStatus$]).pipe(
    map(([products, promoCode]) => {
      if (promoCode.isPromoCodeActive) {
        const fullPricePipe = new ProductPriceFullPipe(new ProductFullPricePipe());

        const fullPrice = fullPricePipe.transform(products);
        const discount = fullPrice * (promoCode.promoCode.percentValue / 100);

        return fullPrice - discount;
      }

      return null;
    })
  );
  
  public postcodeValidMsg = postCodeValidMsg;
  public phoneValidMsg = phoneNumberValidMsg;

  constructor(private store: Store<IState>, private fb: FormBuilder) {}

  public orderProducts(): void {
    this.store.dispatch(orderProductsAction());
  }

  public changeAddress(address: { value: any }): void {
    this.store.dispatch(selectAddressIdAction({ addressId: address.value }));
  }

  public checkPromoCode(): void {
    this.store.dispatch(
      checkPromoCodeAction({
        code: this.orderShipmentForm.get('promoCode').value
      })
    );
  }

  ngOnInit(): void {
    this.store.dispatch(updateShipmentAction({ shipment: null }));
    this.store.dispatch(resetPromoCode());
    this.initForm();
    this.updateAddress();
    this.initForms();
    this.store.dispatch(getShipmentAction());
    this.store.dispatch(getUserAddressesAction());

    this.shipmentPrice$ = combineLatest([this.shipments$, this.orderShipmentForm.get('shipment').valueChanges]).pipe(
      map(([shipments, selectedShipment]) => {
        const shipment = shipments.find(item => item.id === selectedShipment);

        return shipment.price;
      })
    );
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
    this.orderShipmentForm.reset();
  }

  private initForms(): void {
    const subscription = this.basket$.pipe(first()).subscribe((value: IBasketState) => {
      const address = value.address;
      this.orderAddressForm.patchValue({ ...address });
      this.orderShipmentForm.patchValue({
        shipment: value.shipmentId,
        comment: value.comment,
        promoCode: value.promoCode
      });
    });
    this.subscription.add(subscription);
  }

  private updateAddress(): void {
    const sub1 = this.orderAddressForm.valueChanges.subscribe(address => {
      this.store.dispatch(updateAddressAction({ address }));
    });

    const sub2 = this.orderShipmentForm.get('shipment').valueChanges.subscribe(shipment => {
      this.store.dispatch(updateShipmentAction({ shipment }));
    });

    const sub3 = this.orderShipmentForm.get('comment').valueChanges.subscribe(comment => {
      this.store.dispatch(updateCommentAction({ comment }));
    });

    const sub4 = this.orderShipmentForm.get('promoCode').valueChanges.subscribe(promoCode => {
      this.store.dispatch(updatePromoCodeAction({ promoCode }));
    });
    this.subscription.add(sub1);
    this.subscription.add(sub2);
    this.subscription.add(sub3);
    this.subscription.add(sub4);
  }

  private initForm(): void {
    this.orderAddressForm = this.fb.group({
      city: ['', [Validators.required]],
      street: ['', [Validators.required]],
      postCode: ['', [Validators.required, Validators.pattern(/^(\d{5})|(\d{2}\-{1}\d{3})$/)]],
      phoneNumber: ['', [Validators.required, Validators.pattern(/^(\d{9})$/)]],
      country: ['', [Validators.required]]
    });

    this.orderShipmentForm = this.fb.group({
      shipment: [undefined, Validators.required],
      comment: [''],
      promoCode: ['']
    });
  }
}
