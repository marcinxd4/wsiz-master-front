import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderComponent } from './order.component';
import { MaterialModule } from 'src/app/core/material.module';
import { ReactiveFormsModule, FormBuilder } from '@angular/forms';
import { mockStore } from 'src/test/mockstore';
import { MockStore } from '@ngrx/store/testing';
import { IState } from 'src/app/core/interface/redux.interface';
import { Store } from '@ngrx/store';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorMsgDirective } from 'src/app/shared/directives/error-msg.directive';
import { Component } from '@angular/core';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { DummyTableComponent } from 'src/test/dummy/dummy-table-component.mock';
import {
  orderProductsAction,
  selectAddressIdAction,
  checkPromoCodeAction,
  resetPromoCode,
  updateShipmentAction
} from '../../redux/actions';
import { getUserAddressesAction } from 'src/app/core/actions/address.actions';
import { getShipmentAction } from 'src/app/core/actions/shipment.actions';

describe('OrderComponent', () => {
  let component: OrderComponent;
  let fixture: ComponentFixture<OrderComponent>;
  let store: MockStore<IState>;

  beforeEach(async(() => {
    // tslint:disable-next-line: no-floating-promises
    TestBed.configureTestingModule({
      declarations: [OrderComponent, ErrorMsgDirective, DummyTableComponent],
      imports: [MaterialModule, ReactiveFormsModule, BrowserModule, CommonModule, NoopAnimationsModule],
      providers: [mockStore]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    store = TestBed.get(Store);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should dispatch action orderProductsAction', () => {
    const dispatchSpy = spyOn(store, 'dispatch');
    const action = orderProductsAction();
    component.orderProducts();

    expect(dispatchSpy).toHaveBeenCalledWith(action);
  });

  it('should dispatch selectAddressIdAction with param', () => {
    const dispatchSpy = spyOn(store, 'dispatch');
    const action = selectAddressIdAction({ addressId: '2' });

    component.changeAddress({ value: '2' });

    expect(dispatchSpy).toHaveBeenCalledWith(action);
  });

  it('should dispatch checkPromoCodeAction with param', () => {
    const dispatchSpy = spyOn(store, 'dispatch');
    const action = checkPromoCodeAction({ code: 'TEST123' });

    component.orderShipmentForm.get('promoCode').setValue('TEST123');
    component.checkPromoCode();

    expect(dispatchSpy).toHaveBeenCalledWith(action);
  });

  it('should dispatch updateShipmentAction resetPromoCode getShipmentAction getUserAddressesAction', () => {
    const action1 = updateShipmentAction({ shipment: null });
    const action2 = resetPromoCode();
    const action3 = getShipmentAction();
    const action4 = getUserAddressesAction();
    const dispatchSpy = spyOn(store, 'dispatch');

    component.ngOnInit();

    expect(dispatchSpy).toHaveBeenCalledWith(action1);
    expect(dispatchSpy).toHaveBeenCalledWith(action2);
    expect(dispatchSpy).toHaveBeenCalledWith(action3);
    expect(dispatchSpy).toHaveBeenCalledWith(action4);
  });

  it('should reset orderShipmentForma and unsubscribe', () => {
    const dispatchSpy = spyOn(store, 'dispatch');
    const unsubscribeSpy = spyOn(component.subscription, 'unsubscribe');
    const resetSpy = spyOn(component.orderShipmentForm, 'reset');

    component.ngOnDestroy();

    expect(unsubscribeSpy).toHaveBeenCalled();
    expect(resetSpy).toHaveBeenCalled();
  });
});
