import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BasketComponent } from './basket.component';
import { MaterialModule } from 'src/app/core/material.module';
import { mockStore } from 'src/test/mockstore';
import { RouterModule, Router } from '@angular/router';
import { DummyTableComponent } from 'src/test/dummy/dummy-table-component.mock';
import { APP_BASE_HREF } from '@angular/common';
import { MockStore } from '@ngrx/store/testing';
import { IState } from 'src/app/core/interface/redux.interface';
import { Store } from '@ngrx/store';
import { resetBsketAction } from '../../redux/actions';
import { MatSnackBar } from '@angular/material';

describe('BasketComponent', () => {
  let component: BasketComponent;
  let fixture: ComponentFixture<BasketComponent>;
  let store: MockStore<IState>;
  let router: Router;
  let matSnackBarMock: MatSnackBar;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BasketComponent, DummyTableComponent],
      imports: [MaterialModule, RouterModule.forRoot([])],
      providers: [mockStore, { provide: APP_BASE_HREF, useValue: '/' }]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BasketComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    store = TestBed.get(Store);
    router = TestBed.get(Router);
    matSnackBarMock = TestBed.get(MatSnackBar);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should dispatch resetBasketAction', () => {
    const dispatchSpy = spyOn(store, 'dispatch');
    const action = resetBsketAction();

    component.resetBasket();

    expect(dispatchSpy).toHaveBeenCalledWith(action);
  });
  it('should display snackbar with message if basket empty', () => {
    // tslint:disable-next-line: no-string-literal
    component['basketEmpty'] = true;
    const snackbarOpenSpy = spyOn(matSnackBarMock, 'open');

    component.orderProducts();

    expect(snackbarOpenSpy).toHaveBeenCalledWith('Nie można złożyć zamównia jeżeli koszyk jest pusty!', 'Błąd', {
      duration: 4000
    });
  });

  it('should display snackbar with message if basket not empty', () => {
    // tslint:disable-next-line: no-string-literal
    component['basketEmpty'] = false;
    const routerNavigationSpy = spyOn(router, 'navigate');

    component.orderProducts();

    expect(routerNavigationSpy).toHaveBeenCalledWith(['/', 'basket', 'order']);
  });
});
