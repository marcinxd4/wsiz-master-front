import { ChangeDetectionStrategy, Component, OnInit, OnDestroy } from '@angular/core';
import { Store, select } from '@ngrx/store';

import { IState } from '../../../../core/interface/redux.interface';
import { resetBsketAction } from '../../redux/actions';
import { basketProducts } from '../../redux/basket.selectors';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-basket',
  templateUrl: './basket.component.html',
  styleUrls: ['./basket.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BasketComponent implements OnInit, OnDestroy {
  private basketEmpty = true;
  private subscribtion: Subscription;
  constructor(private readonly store: Store<IState>, private router: Router, private snackbar: MatSnackBar) {}

  public resetBasket(): void {
    this.store.dispatch(resetBsketAction());
  }

  public orderProducts(): void {
    if (this.basketEmpty) {
      this.snackbar.open('Nie można złożyć zamównia jeżeli koszyk jest pusty!', 'Błąd', {
        duration: 4000
      });
    } else {
      this.router.navigate(['/', 'basket', 'order']);
    }
  }

  ngOnInit(): void {
    // tslint:disable-next-line: newline-per-chained-call
    this.subscribtion = this.store.pipe(select(basketProducts)).subscribe(v => {
      this.basketEmpty = v.length === 0;
    });
  }

  ngOnDestroy(): void {
    this.subscribtion.unsubscribe();
  }
}
