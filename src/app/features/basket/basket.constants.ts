export const ERROR_CODES = {
  shipmentId: 'Nie wybrano sposobu dostawy zamowienia! Wybierz sposób dostawy i spróbuj jeszcze raz!',
  Address: 'Nie wybrano adresu dostawy! Wybierz adres i  spróbuj jeszcze raz!',
  AddressId: 'Nie wybrano adresu dostawy! Wybierz adres i  spróbuj jeszcze raz!',
  OrderDiscountNotExist: 'Podany kod promocyjny nie istnieje!',
  OrderProductsNotFound: 'Brak produktów w koszyku!'
};
