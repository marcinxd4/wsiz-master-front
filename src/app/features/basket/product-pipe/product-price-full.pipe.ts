import { Pipe, PipeTransform } from '@angular/core';
import { ProductFullPricePipe } from '../../../shared/pipes/product-full-price.pipe';

@Pipe({
  name: 'productPriceFull'
})
export class ProductPriceFullPipe implements PipeTransform {
  constructor(private itemPricePipe: ProductFullPricePipe) {}

  transform(value: any, args?: any): any {
    const pircesList = value
      .map(item => this.itemPricePipe.transform(item))
      .reduce((previousValue, currentValue) => previousValue + currentValue);

    return pircesList;
  }
}
