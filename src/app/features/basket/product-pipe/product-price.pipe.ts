import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'productPrice'
})
export class ProductPricePipe implements PipeTransform {
    transform(value: any, args?: any): any {
        let charmsPrice = 0;
        if (value.charms.length > 0) {
            charmsPrice = value.charms
                .map(charm => charm.price)
                .reduce((previousValue, currentValue) => previousValue + currentValue);
        }

        return value.amount * (value.price + charmsPrice);
    }

}
