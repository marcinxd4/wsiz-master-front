export interface AddressModel {
  city: string;
  street: string;
  postCode: string;
  country: string;
  phoneNumber: string;
}
