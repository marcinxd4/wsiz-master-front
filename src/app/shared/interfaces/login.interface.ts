export interface LoginData {
  email: string;
  password: string;
  rememberMe?: boolean;
  redirectPath?: string;
}

export interface AuthData {
  userId?: string | null;
  email?: string | null;
  token?: string | null;
  expires?: string | null;
  userType?: string | null;
  refreshToken?: string | null;
  rememberMe?: boolean | null;
  error?: string | null;
}
