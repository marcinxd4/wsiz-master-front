import { IShipment } from "src/app/core/interface/shipment.interface";
import { IAddress } from "src/app/core/interface/order.interface";
import { IProduct } from "src/app/core/interface/redux.interface";

export interface ErrorMsgModel {
  [key: string]: string;
}

export interface IDiscount {
  id: string;
  name: string;
  nameEng: string;
  description: string;
  descriptionEng: string;
  percentValue: number;
  isActive: boolean;
  availableFrom: string | Date;
  availableTo: string | Date;
  categoryIds: string[];
}

export interface IOrderUser {
  id: string;
  email: string;
  firstName: string;
  lastName: string;
  fullName: string;
  creationDateTime: string;
  phoneNumber: number | string;
}

export interface IProductOrder {
  id: string;
  amount: number;
  currentProductPrice: number;
  finalPrice: number;
  comment: string;
  charmsPrice: number;
  productCategoryDiscount: IDiscount;
  product: IProduct;
  productOrderCharms: any[];
}

export interface IOrderItem {
  id: number;
  isShipped: boolean;
  isPayed: boolean;
  isClosed: boolean;
  comment: string;
  basePrice: number;
  shipmentPrice: number;
  finalPrice: number;
  creationDateTime: string | Date;
  payedDateTime: string | Date;
  shipmentDateTime: string | Date;
  closedDateTime: string | Date;
  shipment: IShipment;
  user: IOrderUser;
  address: IAddress;
  orderDiscount: any;
  productOrders: IProductOrder[];
}
