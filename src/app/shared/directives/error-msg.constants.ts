import { ErrorMsgModel } from '../interfaces/common.interface';

export const INVALID_EMAIL = 'Proszę wpisac poprawny address email';
export const EMAIL_REQUIRED = 'Prosze wpisać email';
export const PASSWORD_REQ = 'Proszę wpisać hasło';
export const PASSWORD_INVALID = 'Proszę wpisac poprawne hasło';
export const PASSWORD_CONFIRMATION_INVALID = 'Hasła nie pasują do siebie, proszę wpisac poprawne hasło';
export const INVALID = 'Proszę wpisac poprawną wartość';
export const REQUIRED = 'Proszę uzupełnić, pole jest wymagane';
export const INVALID_POSTCODE = 'Nieprawidlowy kod pocztowy';
export const INVALID_PHONE = 'Nieprawidlowy numer telefonu';
export const REGISTRATIONS = 'Zaakceptuj regulamin';

export const errConfig = {
  email: INVALID_EMAIL,
  required: REQUIRED,
  pattern: INVALID,
  NoPasswordMatch: PASSWORD_CONFIRMATION_INVALID
};

export const postCodeValidMsg: ErrorMsgModel = {
  pattern: INVALID_POSTCODE
};

export const phoneNumberValidMsg: ErrorMsgModel = {
  pattern: INVALID_PHONE
};

export const passwordValidMsg: ErrorMsgModel = {
  minlength: PASSWORD_INVALID,
  maxlength: PASSWORD_INVALID,
  required: PASSWORD_INVALID
};

export const regulationsError: ErrorMsgModel = {
  required: REGISTRATIONS
};
