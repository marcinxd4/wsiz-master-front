import { AbstractControl } from '@angular/forms';
import { Directive, Input, OnInit, ElementRef, OnDestroy } from '@angular/core';

import { errConfig } from 'src/app/shared/directives/error-msg.constants';
import { Subscription } from 'rxjs';

@Directive({
  selector: '[appErrorMsg]'
})
export class ErrorMsgDirective implements OnInit, OnDestroy {
  @Input() control: AbstractControl;
  @Input() customErr: { [key: string]: string };

  private subscription: Subscription;
  constructor(private el: ElementRef) {}

  ngOnInit(): void {
    this.subscription = this.control.statusChanges.subscribe((value: any) => {
      this.validateForm();
    });
  }
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
  private validateForm(): void {
    this.el.nativeElement.innerText = this.getMessage();
  }

  private getMessage(): string {
    const { errors } = this.control;
    const key: string = errors ? Object.keys(errors)[0] : null;
    console.log(errors);
    if (!!this.customErr && !!key && !!this.customErr[key]) {
      return this.customErr[key];
    }

    return errConfig[key] || null;
  }
}
