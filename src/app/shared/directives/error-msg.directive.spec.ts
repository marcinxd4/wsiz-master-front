import { ErrorMsgDirective } from './error-msg.directive';
import { ElementRef } from '@angular/core';

describe('ErrorMsgDirective', () => {
  it('should create an instance', () => {
    const elementRef = new ElementRef('');
    const directive = new ErrorMsgDirective(elementRef);
    expect(directive).toBeTruthy();
  });
});
