import { NgModule } from '@angular/core';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatIconModule } from '@angular/material/icon';
import { CommonModule } from '@angular/common';

import { MaterialModule } from '../core/material.module';

import { DiscountPipe } from './pipes/discount.pipe';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ProductFullPricePipe } from './pipes/product-full-price.pipe';
import { ProductPreviewComponent } from './components/product-preview/product-preview.component';
import { NoticeDialogComponent } from './component/notice-dialog/notice-dialog.component';
import { GalleryComponent } from './components/gallery/gallery.component';
import { SliderComponent } from './components/slider/slider.component';
import { CarouselModule } from 'ngx-bootstrap/carousel';
import { OrderCharmsPipe } from './pipes/order-charms.pipe';
import { ErrorMsgDirective } from './directives/error-msg.directive';

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, MaterialModule, CarouselModule],
  declarations: [
    DiscountPipe,
    ProductFullPricePipe,
    ProductPreviewComponent,
    NoticeDialogComponent,
    GalleryComponent,
    SliderComponent,
    OrderCharmsPipe,
    ErrorMsgDirective
  ],
  providers: [ProductFullPricePipe],
  entryComponents: [ProductPreviewComponent, NoticeDialogComponent],
  exports: [
    CommonModule,
    DiscountPipe,
    ProductFullPricePipe,
    MatIconModule,
    MatGridListModule,
    ProductPreviewComponent,
    NoticeDialogComponent,
    GalleryComponent,
    SliderComponent,
    OrderCharmsPipe,
    ErrorMsgDirective
  ]
})
export class SharedModule {}
