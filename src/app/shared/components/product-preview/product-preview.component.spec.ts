import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductPreviewComponent } from './product-preview.component';
import { MaterialModule } from 'src/app/core/material.module';
import { GalleryComponent } from '../gallery/gallery.component';
import { ProductPriceFullPipe } from 'src/app/features/basket/product-pipe/product-price-full.pipe';
import { ProductFullPricePipe } from '../../pipes/product-full-price.pipe';

xdescribe('ProductPreviewComponent', () => {
  let component: ProductPreviewComponent;
  let fixture: ComponentFixture<ProductPreviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProductPreviewComponent, GalleryComponent, ProductPriceFullPipe, ProductFullPricePipe],
      imports: [MaterialModule]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductPreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
