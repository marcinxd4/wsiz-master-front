import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { IProductBasketItem } from 'src/app/core/interface/redux.interface';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-product-preview',
  templateUrl: './product-preview.component.html',
  styleUrls: ['./product-preview.component.scss']
})
export class ProductPreviewComponent implements OnInit {
  public readonly host: string = environment.HOST;
  public pictureIndex = 0;

  constructor(@Inject(MAT_DIALOG_DATA) public data: IProductBasketItem) {}

  public switchPhoto(index: number): void {
    this.pictureIndex = index;
  }

  ngOnInit(): void {}
}
