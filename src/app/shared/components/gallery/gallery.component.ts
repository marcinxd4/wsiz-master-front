import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss']
})
export class GalleryComponent {
  @Input() pictures: { imageUrl: string, isMain: boolean }[];
  @Output() selectPicture = new EventEmitter<number>();
 
  public host = environment.HOST;
  
  private activeIndex = 0;
  constructor() { }

  public selectImage(index: number): void {
    this.activeIndex = index;
    this.selectPicture.emit(index);
  }
}
