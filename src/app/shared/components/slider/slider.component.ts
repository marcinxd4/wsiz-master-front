import { Component, OnInit } from "@angular/core";
import { trigger, state, style, animate, transition } from "@angular/animations";
// tslint:disable: quotemark

const SLIDE_STATE = {
  FIRST: "first-slide",
  SECOND: "second-slide",
  THIRD: "third-slide"
};
@Component({
  selector: "app-slider",
  templateUrl: "./slider.component.html",
  styleUrls: ["./slider.component.scss"],
  animations: [
    trigger("slide", [
      state(SLIDE_STATE.FIRST, style({ transform: "translateX(0)" })),
      state(SLIDE_STATE.SECOND, style({ transform: "translateX(-100vw)" })),
      state(SLIDE_STATE.THIRD, style({ transform: "translateX(-200vw)" })),
      transition("* => *", animate(500))
    ])
  ]
})
export class SliderComponent implements OnInit {
  public slides = {
    first: "assets/images/1.jpg",
    second: "assets/images/2.jpg",
    third: "assets/images/3.jpg"
  };

  slides2 = ["assets/images/1.jpg", "assets/images/2.jpg", "assets/images/3.jpg"];
  public stateSlider = SLIDE_STATE.FIRST;
  public index = 0;

  ngOnInit(): void {
    setInterval(() => {
      if (SLIDE_STATE.FIRST === this.stateSlider) {
        this.stateSlider = SLIDE_STATE.SECOND;
      } else if (SLIDE_STATE.SECOND === this.stateSlider) {
        this.stateSlider = SLIDE_STATE.THIRD;
      } else if (SLIDE_STATE.THIRD === this.stateSlider) {
        this.stateSlider = SLIDE_STATE.FIRST;
      }
    }, 4000);
  }
}
