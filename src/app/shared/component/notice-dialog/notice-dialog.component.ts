import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-notice-dialog',
  templateUrl: './notice-dialog.component.html',
  styleUrls: ['./notice-dialog.component.scss']
})
export class NoticeDialogComponent {
  constructor(
    public dialogRef: MatDialogRef<NoticeDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {}

}
