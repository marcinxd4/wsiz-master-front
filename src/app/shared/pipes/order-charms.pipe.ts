import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'orderCharms'
})
export class OrderCharmsPipe implements PipeTransform {
  transform(value: any[]): any {
    const array = value;
    const compare = (a, b) => {
      if (a.sequence > b.sequence) { return 1; }
      if (a.sequence < b.sequence) { return -1; }

      return 0;
    };

    return value.length > 1 ? array.sort() : value;
  }
}
