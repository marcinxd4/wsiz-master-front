import { Pipe, PipeTransform } from '@angular/core';
import { IProductBasketItem } from '../../core/interface/redux.interface';

@Pipe({
  name: 'productFullPrice'
})
export class ProductFullPricePipe implements PipeTransform {
  transform(value: IProductBasketItem): any {
    const { product, charms } = value;
    const charmsToPay = charms.filter(v => !v.forFree);
    const productPrice = !!product.discount
      ? product.price - product.price * (product.discount.percentValue / 100)
      : product.price;
    const charmsPrice =
      charmsToPay.length > 0
        ? charmsToPay
            .map(item =>
              !!product.discount
                ? item.charm.price - item.charm.price * (product.discount.percentValue / 100)
                : item.charm.price
            )
            .reduce((sum, item) => sum + item, 0)
        : 0;

    return (productPrice + charmsPrice) * value.amount;
  }
}
