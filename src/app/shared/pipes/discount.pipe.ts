import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'discount' })
export class DiscountPipe implements PipeTransform {
  transform(value: number, discount: any): any {
    const price = value - (value * (discount.percentValue / 100));
    
    return price;
  }
}
