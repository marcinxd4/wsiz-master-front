/* tslint:disable */
export const moveArrayItem = (array: any[], prev: number, next: number): any[] => {
  const item = array[prev];
  const newArray = [...array.slice(0, prev), ...array.slice(prev +1)];
  const final = [...newArray.slice(0, next), item, ...newArray.slice(next)];

  return final;
};
