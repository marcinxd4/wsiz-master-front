export const CONFIRMATION_DIALOG = {
  TITLE: 'Czy napewno chcesz usunąć produkt?',
  MESSAGE: 'Czy jesteś pewien że chcesz usunąć ten produkt?'
};
