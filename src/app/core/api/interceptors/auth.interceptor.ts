import {
  HttpClient,
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpHeaders,
  HttpInterceptor,
  HttpRequest
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { IState } from '../../interface/redux.interface';
import { select, Store } from '@ngrx/store';
import { ADDRESS_URL, ORDERS_URL, ORDER_DISCOUNT_URL } from '../api.constatns';
import { authDataSelector } from '../../../features/login/redux/login.selectors';
import { catchError, filter, first, switchMap, take } from 'rxjs/operators';
import { AuthData } from '../../../shared/interfaces/login.interface';
import { signInSuccessAction, signOutAction } from '../../../features/login/redux/login.actions';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  private refreshTokenInProgress = false;
  private refreshTokenSubject: BehaviorSubject<any> = new BehaviorSubject<any>(null);

  constructor(private store: Store<IState>, private httpClinet: HttpClient) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return this.store.pipe(
      select(authDataSelector),
      first(),
      switchMap((data: AuthData) => {
        if (req.url.match(ORDERS_URL) || req.url.match(ADDRESS_URL) || req.url.match(ORDER_DISCOUNT_URL)) {
          const request = req.clone({
            headers: new HttpHeaders({
              'Content-Type': 'application/json',
              Accept: 'application/json',
              Authorization: `Bearer ${data.token}`
            })
          });

          return next.handle(request).pipe(
            catchError(error => {
              if (request.url.includes('api/auth/refresh') || request.url.includes('api/auth')) {
                if (request.url.includes('api/auth/refresh')) {
                  this.store.dispatch(signOutAction());
                }

                return throwError(error);
              }

              if (error.status !== 401) {
                return throwError(error);
              }

              if (this.refreshTokenInProgress) {
                this.refreshTokenSubject.pipe(
                  filter(value => value !== null),
                  take(1),
                  switchMap(token => {
                    const newRequest = req.clone({
                      headers: new HttpHeaders({
                        'Content-Type': 'application/json',
                        Accept: 'application/json',
                        Authorization: `Bearer ${token}`
                      })
                    });

                    return next.handle(newRequest);
                  })
                );
              } else {
                this.refreshTokenInProgress = true;
                // tslint:disable-next-line
                this.refreshTokenSubject.next(null);

                return this.httpClinet
                  .post('api/auth/refresh', { jwtToken: data.token, refreshToken: data.refreshToken })
                  .pipe(
                    switchMap((response: AuthData) => {
                      this.refreshTokenInProgress = false;
                      this.refreshTokenSubject.next(response.token);
                      this.store.dispatch(signInSuccessAction({ data: response }));
                      const newRequest = req.clone({
                        headers: new HttpHeaders({
                          'Content-Type': 'application/json',
                          Accept: 'application/json',
                          Authorization: `Bearer ${response.token}`
                        })
                      });

                      return next.handle(newRequest);
                    }),
                    catchError((error: HttpErrorResponse) => {
                      this.refreshTokenInProgress = false;
                      this.store.dispatch(signOutAction());

                      return throwError(error);
                    })
                  );
              }
            })
          );
        }

        return next.handle(req);
      })
    );
  }
}
