import { Injectable } from "@angular/core";
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from "@angular/common/http";
import { Observable } from "rxjs";
import { SpinnerService } from "../../services/spinner/spinner.service";
import { tap, finalize, delay } from "rxjs/operators";

@Injectable()
export class SpinnerInterceptor implements HttpInterceptor {
    constructor(private spinnerService: SpinnerService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        this.spinnerService.setSpinnerStatus(true);

        return next.handle(request).pipe(
            delay(30),
            finalize(() => this.spinnerService.setSpinnerStatus(false))
        )
    }
}