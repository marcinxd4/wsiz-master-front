import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavbarComponent } from './navbar.component';
import { mockStore } from 'src/test/mockstore';
import { MaterialModule } from '../../material.module';
import { RouterModule } from '@angular/router';
import { APP_BASE_HREF } from '@angular/common';
import { MockStore } from '@ngrx/store/testing';
import { IState } from '../../interface/redux.interface';
import { signOutAction } from 'src/app/features/login/redux/login.actions';
import { Store } from '@ngrx/store';
import { getCategoriesListAction } from '../../actions/categories.actions';
import { of } from 'rxjs';

describe('NavbarComponent', () => {
  let component: NavbarComponent;
  let fixture: ComponentFixture<NavbarComponent>;
  let store: MockStore<IState>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NavbarComponent],
      imports: [MaterialModule, RouterModule.forRoot([])],
      providers: [mockStore, { provide: APP_BASE_HREF, useValue: '/' }]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    store = TestBed.get(Store);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should dispatch action to signOut user', () => {
    const dispatchSpy = spyOn(store, 'dispatch');
    const action = signOutAction();

    component.logoutUser();

    expect(dispatchSpy).toHaveBeenCalledWith(action);
  });

  it('should  dispatch get getCategoriesListAction', () => {
    const dispatchSpy = spyOn(store, 'dispatch');
    const action = getCategoriesListAction();

    component.ngOnInit();

    expect(dispatchSpy).toHaveBeenCalledWith(action);
  });
  it('should set isLoggedUser to true', () => {
    component.user$ = of({ test: ' test' } as any);

    component.ngOnInit();

    component.user$.subscribe(() => {
      expect(component.isLoggedInUser).toBe(true);
    });
  });
  it('should set isLoggedUser to false', () => {
    component.user$ = of({} as any);

    component.ngOnInit();

    component.user$.subscribe(() => {
      expect(component.isLoggedInUser).toBe(false);
    });
  });
  it('should unsubscribe subscriptions in ngOnDestroy method', () => {
    // tslint:disable-next-line: no-string-literal
    const unsubscribeSpy = spyOn(component.subscription, 'unsubscribe');
    component.ngOnDestroy();
    expect(unsubscribeSpy).toHaveBeenCalled();
  });
});
