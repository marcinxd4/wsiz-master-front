import { NOT_LOGGED_USER, LOOGED_USER } from './navbar-config';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { faUserCircle } from '@fortawesome/free-solid-svg-icons';
import { Store, select } from '@ngrx/store';
import { isEmpty } from 'lodash';
import { Observable, Subscription } from 'rxjs';
import { getCategoriesListAction } from '../../actions/categories.actions';
import { categoriesListSelecor } from '../../selectors/categories-list.selector';
import { IState } from '../../interface/redux.interface';
import { AuthData } from '../../../shared/interfaces/login.interface';
import { isAuthenticatedSelector, badgeContentCount } from '../../../features/login/redux/login.selectors';
import { signOutAction } from '../../../features/login/redux/login.actions';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit, OnDestroy {
  public icons = {
    user: faUserCircle
  };
  public subscription: Subscription;
  public isLoggedInUser = false;
  public categoriesList$: Observable<any> = this.store.pipe(select(categoriesListSelecor));
  public user$: Observable<AuthData> = this.store.pipe(select(isAuthenticatedSelector));
  public basketProductCount$: Observable<number> = this.store.pipe(select(badgeContentCount));

  public readonly anonymousUser = NOT_LOGGED_USER;

  public readonly loggedUser = LOOGED_USER;

  constructor(private store: Store<IState>) {}

  public logoutUser(): void {
    this.store.dispatch(signOutAction());
  }

  ngOnInit(): void {
    this.store.dispatch(getCategoriesListAction());
    this.subscription = this.user$.subscribe(user => {
      this.isLoggedInUser = !isEmpty(user);
    });
  }
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
