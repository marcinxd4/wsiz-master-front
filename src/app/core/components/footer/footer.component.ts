import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { SpinnerService } from '../../services/spinner/spinner.service';
import { Observable } from 'rxjs';
import { IState } from '../../interface/redux.interface';
import { Store, select } from '@ngrx/store';
import { categoriesListSelecor } from '../../selectors/categories-list.selector';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FooterComponent implements OnInit {
  public categoriesList$: Observable<any> = this.store.pipe(select(categoriesListSelecor));
  constructor(private store: Store<IState>) {}

  public subscribeNewsletter(): void {}

  ngOnInit(): void {
  }
}
