import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigmDialogComponent } from './configm-dialog.component';
import { MaterialModule } from '../../material.module';
import { MatDialogRef } from '@angular/material';

xdescribe('ConfigmDialogComponent', () => {
  let component: ConfigmDialogComponent;
  let fixture: ComponentFixture<ConfigmDialogComponent>;
  let matDialogRefMock: MatDialogRef<any>;

  beforeEach(async(() => {
    // tslint:disable-next-line: no-floating-promises
    TestBed.configureTestingModule({
      declarations: [ConfigmDialogComponent],
      imports: [MaterialModule],
      providers: [
        {
          provide: MatDialogRef,
          useValue: jasmine.createSpyObj('MatDialogRef', ['close'])
        }
      ]
      // tslint:disable-next-line: newline-per-chained-call
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigmDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    matDialogRefMock = TestBed.get('MatDialogRef');
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('closeDialog invoke close method', () => {
    component.closeDialog();

    expect(matDialogRefMock.close).toHaveBeenCalled();
  });
});
