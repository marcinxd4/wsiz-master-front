import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";

export interface ConfirmationDialogModel {
  title: string;
  message: string;
}

@Component({
  selector: 'app-configm-dialog',
  templateUrl: './configm-dialog.component.html',
  styleUrls: ['./configm-dialog.component.scss']
})
export class ConfigmDialogComponent {
  constructor(public dialogRef: MatDialogRef<ConfigmDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ConfirmationDialogModel) { }

  public closeDialog(): void {
    this.dialogRef.close();
  }
}
