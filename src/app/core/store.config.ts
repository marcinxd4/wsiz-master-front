import { authReducer } from '../features/login/redux';
import { charmsReducer } from '../features/category/redux/charms.reducer';
import { categoryListReducer } from './reducer/category-list.reducer';
import { basketReducer } from '../features/basket/redux/reducer';
import { ActionReducerMap, MetaReducer, ActionReducer } from '@ngrx/store';
import { IState } from './interface/redux.interface';
import { localStorageSync } from 'ngrx-store-localstorage';
import { shipmentReducer } from './reducer/shipment.reducer';
import { addressReducer } from './reducer/adddress.reducer';
import { profileReducer } from '../features/profile/redux/reducer';
import { categoryProductsReducer } from '../features/category/redux/product-category.reducer';

export function localStorageSyncReducer(reducer: ActionReducer<IState, any>) {
  return localStorageSync({
    keys: ['authentication', 'basket'],
    rehydrate: true
  })(reducer);
}

export const metaReducers: Array<MetaReducer<IState, any>> = [localStorageSyncReducer];

export const coreReducers: ActionReducerMap<IState> = {
  authentication: authReducer,
  charms: charmsReducer,
  categories: categoryListReducer,
  products: categoryProductsReducer,
  basket: basketReducer,
  shipment: shipmentReducer,
  address: addressReducer,
  profile: profileReducer
};
