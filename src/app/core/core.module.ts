import { HttpClientModule } from '@angular/common/http';
import { SpinnerComponent } from './components/spinner/spinner.component';
import { FooterComponent } from './components/footer/footer.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { BsDropdownModule } from 'ngx-bootstrap';
import { ToastrModule } from 'ngx-toastr';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { interceptorsList } from './api/interceptors';
import { CategoriesListEffects } from './effects/categories-list.effects';
import { RouterModule } from '@angular/router';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MAT_SNACK_BAR_DEFAULT_OPTIONS } from '@angular/material/snack-bar';
import { MaterialModule } from './material.module';
import { ConfigmDialogComponent } from './components/configm-dialog/configm-dialog.component';
import { coreReducers } from './store.config';
import { metaReducers } from './store.config';
import { NgxImageZoomModule } from 'ngx-image-zoom';
import { BasketEffect } from '../features/basket/redux/effect';
import { ShipmentsEffects } from './effects/shipment.effects';
import { AddressEffects } from './effects/address.effects';
import { SuccessDialogComponent } from './components/success-dialog/success-dialog.component';
import { ProfileEffect } from '../features/profile/redux/effects';
import { ReactiveFormsModule } from '@angular/forms';
import { StoreRouterConnectingModule } from '@ngrx/router-store';
@NgModule({
  imports: [
    CommonModule,
    BrowserAnimationsModule,
    HttpClientModule,
    RouterModule,
    MaterialModule,
    ReactiveFormsModule,
    StoreModule.forRoot(coreReducers, {
      metaReducers,
      runtimeChecks: {
        strictStateImmutability: true,
        strictActionImmutability: true,
        strictStateSerializability: false,
        strictActionSerializability: true
      }
    }),
    StoreDevtoolsModule.instrument({
      maxAge: 10
    }),
    EffectsModule.forRoot([CategoriesListEffects, BasketEffect, ShipmentsEffects, AddressEffects, ProfileEffect]),
    NgxImageZoomModule.forRoot(),
    FontAwesomeModule,
    MatProgressSpinnerModule,

    // Bootstrap
    BsDropdownModule.forRoot()
  ],
  declarations: [NavbarComponent, FooterComponent, SpinnerComponent, ConfigmDialogComponent, SuccessDialogComponent],
  providers: [interceptorsList, { provide: MAT_SNACK_BAR_DEFAULT_OPTIONS, useValue: { duration: 2500 } }],
  entryComponents: [ConfigmDialogComponent, SuccessDialogComponent],
  exports: [
    NavbarComponent,
    BsDropdownModule,
    FontAwesomeModule,
    SuccessDialogComponent,
    FooterComponent,
    SpinnerComponent,
    ToastrModule
  ]
})
export class CoreModule {}
