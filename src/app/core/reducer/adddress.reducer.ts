import {
  getUserAddressesAction,
  getUserAddressesFailureAction,
  getUserAddressesSuccessAction
} from '../actions/address.actions';
import { IUserAddress } from '../interface/address.interface';

export const initAddress: IUserAddress = {
  addresses: [],
  loading: false,
  error: null
};

export function addressReducer(state: IUserAddress = initAddress, action): IUserAddress {
  switch (action.type) {
    case getUserAddressesAction.type:
      return {
        ...state,
        loading: true
      };
    case getUserAddressesSuccessAction.type:
      return {
        ...state,
        loading: false,
        addresses: action.addresses
      };
    case getUserAddressesFailureAction.type:
      return {
        ...state,
        loading: false,
        error: action.error
      };

    default:
      return { ...state };
  }
}
