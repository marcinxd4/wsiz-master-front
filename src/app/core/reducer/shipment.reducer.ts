import { IShipment } from '../interface/shipment.interface';
import { getShipmentAction, getShipmentSuccessAction, getShipmentFailureAction } from '../actions/shipment.actions';

export const initialState: IShipmentState = {
  shipments: [],
  error: null,
  loading: false
};

export interface IShipmentState {
  shipments: IShipment[];
  error: string | null;
  loading: boolean;
}

export function shipmentReducer(state: IShipmentState = initialState, action): IShipmentState {
  switch (action.type) {
    case getShipmentAction.type:
      return {
        ...state,
        loading: true
      };
    case getShipmentSuccessAction.type:
      return {
        ...state,
        loading: false,
        shipments: action.shipments
      };
    case getShipmentFailureAction.type:
      return {
        ...state,
        loading: false,
        error: action.error
      };
    default:
      return { ...state };
  }
}
