import { ICategoriesProducts } from '../interface/redux.interface';
import {
  getCategoriesListSuccessActon,
  getCategoriesListFailureActon,
  getCategoriesListAction
} from '../actions/categories.actions';

export const initStateCatProducts: ICategoriesProducts = {
  categories: [],
  error: null,
  loading: false
};
export function categoryListReducer(state: ICategoriesProducts = initStateCatProducts, action): ICategoriesProducts {
  switch (action.type) {
    case getCategoriesListAction.type:
      return {
        ...state,
        loading: true
      };
    case getCategoriesListSuccessActon.type:
      return {
        ...state,
        categories: [...action.categories],
        error: null,
        loading: false
      };
    case getCategoriesListFailureActon.type:
      return {
        ...state,
        error: action.error,
        loading: false
      };
    default:
      return state;
  }
}
