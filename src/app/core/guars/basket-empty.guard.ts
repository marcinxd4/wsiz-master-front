import { IState } from '../interface/redux.interface';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivate } from '@angular/router';
import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BasketEmptyGuard implements CanActivate {
  basketProducts$: Observable<any>;
  constructor(private store: Store<IState>, private router: Router) {
    this.basketProducts$ = this.store.pipe(select((state: IState) => state.basket.products));
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    return this.basketProducts$.pipe(
      map(products => {
        if (products.length > 0) {
          return true;
        } else {
          this.router.navigate(['/', 'basket']);

          return false;
        }
      })
    );
  }
}
