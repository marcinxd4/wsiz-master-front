import { map, debounceTime } from 'rxjs/operators';
import { AuthData } from './../../shared/interfaces/login.interface';
import { Store, select } from '@ngrx/store';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { IState } from '../interface/redux.interface';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  private userData$: Observable<AuthData>;

  constructor(private store: Store<IState>, private router: Router, private activateRoute: ActivatedRoute) {
    this.userData$ = this.store.pipe(select((state: IState) => state.authentication));
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    return this.userData$.pipe(
      debounceTime(200),
      map((user: AuthData) => {
        if (
          user.token !== null &&
          user.userId !== null &&
          user.email !== null &&
          user.expires !== null &&
          user.refreshToken !== null &&
          user.userType !== null
        ) {
          return true;
        }
        this.router.navigate(['login'], {
          queryParams: { returnUrl: state.url }
        });

        return false;
      })
    );
  }
}
