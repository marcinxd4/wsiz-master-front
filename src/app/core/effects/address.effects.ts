import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { getShipmentAction, getShipmentFailureAction, getShipmentSuccessAction } from '../actions/shipment.actions';
import { catchError, distinctUntilChanged, map, switchMap, take } from 'rxjs/operators';
import { IShipment } from '../interface/shipment.interface';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { getUserAddressesAction, getUserAddressesFailureAction, getUserAddressesSuccessAction } from '../actions/address.actions';
import { ADDRESS_URL } from '../api/api.constatns';

@Injectable()
export class AddressEffects {
  addressEffect$ = createEffect(() =>
    this.actions$.pipe(
      ofType(getUserAddressesAction.type),
      take(1),
      switchMap(() => {
        return this.httpClient.get(ADDRESS_URL)
          .pipe(
            distinctUntilChanged(),
            map((addresses: any[]) => getUserAddressesSuccessAction({addresses})),
            catchError((error: HttpErrorResponse) => of(getUserAddressesFailureAction({error: error.message})))
          );
      })
    )
  );

  constructor(private actions$: Actions,
              private httpClient: HttpClient) {
  }
}
