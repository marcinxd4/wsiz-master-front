import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, distinctUntilChanged, map, switchMap, take } from 'rxjs/operators';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { getShipmentAction, getShipmentFailureAction, getShipmentSuccessAction } from '../actions/shipment.actions';
import { IShipment } from '../interface/shipment.interface';
import { SHIPMENTS_URL } from '../api/api.constatns';

@Injectable()
export class ShipmentsEffects {
  categoriesList$ = createEffect(() =>
    this.actions$.pipe(
      ofType(getShipmentAction.type),
      take(1),
      switchMap(() => {
        return this.httpClient.get(SHIPMENTS_URL)
          .pipe(
            distinctUntilChanged(),
            map((shipments: IShipment[]) => shipments.filter(shipment => shipment.isActive)),
            map((shipments: IShipment[]) => getShipmentSuccessAction({shipments})),
            catchError((error: HttpErrorResponse) => of(getShipmentFailureAction({error: error.message})))
          );
      })
    )
  );

  constructor(private actions$: Actions,
              private httpClient: HttpClient) {
  }
}
