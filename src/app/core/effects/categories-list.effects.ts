import { Injectable } from '@angular/core';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { catchError, distinctUntilChanged, map, switchMap } from 'rxjs/operators';
import { getCategoriesListAction, getCategoriesListSuccessActon, getCategoriesListFailureActon } from '../actions/categories.actions';
import { of } from 'rxjs';

@Injectable()
export class CategoriesListEffects {
  categoriesList$ = createEffect(() =>
    this.actions$.pipe(
      ofType(getCategoriesListAction.type),
      switchMap(() => {
        return this.httpClient.get('api/productCategories')
          .pipe(
            distinctUntilChanged(),
            map((categories: any) => getCategoriesListSuccessActon({ categories })),
            catchError((error: HttpErrorResponse) => of(getCategoriesListFailureActon({ error: error.message })))
          );
      })
    )
  );

  constructor(private actions$: Actions,
    private httpClient: HttpClient) {
  }
}
