import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { filter } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SpinnerService {
  private showSpinner: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  public getSpinnerStatus(): Observable<boolean> {
    return this.showSpinner.pipe(filter(v => v !== null));
  }

  public setSpinnerStatus(status: boolean): void {
    this.showSpinner.next(status);
  }
}
