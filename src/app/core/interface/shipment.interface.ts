export interface IShipment {
  id: string;
  name: string;
  nameEng: string;
  description: string;
  descriptionEng: string;
  price: number;
  isActive: string;
  type: number;
}
