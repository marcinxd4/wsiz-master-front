import { IShipmentState } from '../reducer/shipment.reducer';
import { AuthData } from '../../shared/interfaces/login.interface';
import { IBasketState } from '../../features/basket/redux/state';
import { IUserAddress } from './address.interface';
import { IProfileState } from 'src/app/features/profile/redux/state';
import { RouterReducerState } from '@ngrx/router-store';

export interface IState {
  authentication: AuthData;
  charms?: ICharmsState;
  categories?: ICategoriesProducts;
  basket?: IBasketState;
  orders?: any;
  products?: IProducts;
  shipment?: IShipmentState;
  address?: IUserAddress;
  profile?: IProfileState;
}

export interface ICategoriesProducts {
  categories: IProductCategory[];
  error: string;
  loading: boolean;
}

export interface ICharmsState {
  charms: ICharmCategory[];
  error: string | null;
  loading: boolean;
}

export interface IProducts {
  products: IProduct[];
  edited: IProductBasketItem;
  error: string;
  loading: boolean;
}

export interface IProductBasketItem {
  product: IProduct | null;
  charms: IProductCharm[];
  comment: string | null;
  amount: number | null;
  index?: number;
}

// ====================================

export interface IProduct {
  discount: any;
  id: number | string;
  name: string;
  nameEng: string;
  price: number;
  status: number;
  description: string;
  descriptionEng: string;
  acceptCharms: boolean;
  maxCharmsCount: number;
  sizes: any;
  images: IImage[];
  productCategoryId: string;
  product?: any;
  comment?: string;
}

export interface IImage {
  imageUrl: string;
  isMain: boolean;
}

export interface ICharm {
  id: string;
  name: string;
  nameEng: string;
  imagePath: string;
  price: number;
  status: number;
}

export interface IProductCharm {
  charm: ICharm;
  sequence: number;
  forFree: boolean;
}

export interface ICharmCategory {
  id: string;
  name: string;
  nameEng: string;
  charms: ICharm[];
}

export interface IProductCategory {
  id: string;
  name: string;
  nameEng: string;
  productCategories: IProductCategory[];
  productCategoryDiscount: any;
}
