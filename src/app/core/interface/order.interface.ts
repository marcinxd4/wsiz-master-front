export interface IOrder {
  products: IOrderProduct[];
  shipmentId: string | number;
  address: IAddress;
  addressId: string;
  comment: string;
  promoCode: string;
}

export interface IOrderProduct {
  productId: number | string;
  amount: number;
  comment: string;
  charms: { charmId: string; sequence: number }[];
}

export interface IAddress {
  city: string;
  street: string;
  postCode: string;
  phoneNumber: any;
  country: string;
  id?: string;
}
