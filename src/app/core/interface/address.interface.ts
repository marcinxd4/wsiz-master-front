import { IAddress } from './order.interface';

export interface IUserAddress {
  addresses: IAddress[];
  loading: boolean;
  error: string | null;
}
