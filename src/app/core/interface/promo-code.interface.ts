export interface IPromoCode {
  availableFrom: Date | string;
  availableTo: Date | string;
  code: string;
  description: string;
  descriptionEng: string;
  id: number | string;
  isActive: boolean;
  name: string;
  nameEng: string;
  percentValue: number;
}

export interface IPromoCodeStatus {
  loading: boolean;
  promoCode: IPromoCode;
  isPromoCodeActive: boolean;
  error: string;
}
