import { IState } from '../interface/redux.interface';
import { createSelector } from '@ngrx/store';

export const shipmentFeature = (state: IState) => state.shipment;

export const getShipments = createSelector(
  shipmentFeature,
  details => details.shipments
);

export const getShipmentsLoading = createSelector(
  shipmentFeature,
  details => details.shipments
);

export const getShipmentError = createSelector(
  shipmentFeature,
  details => details.shipments
);
