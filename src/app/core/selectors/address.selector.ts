import { IState } from '../interface/redux.interface';
import { createSelector } from '@ngrx/store';

export const addressFeature = (state: IState) => state.address;

export const addressesListSelector = createSelector(
  addressFeature,
  details => details.addresses
);

export const addressLoadingSelector = createSelector(
  addressFeature,
  details => details.loading
);

export const addressErrorSelector = createSelector(
  addressFeature,
  details => details.error
);
