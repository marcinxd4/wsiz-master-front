import { IState, ICategoriesProducts } from "../interface/redux.interface";
import { createSelector } from "@ngrx/store";

export const categoriesListFeature = (state: IState) => state.categories

export const categoriesListSelecor = createSelector(
    categoriesListFeature,
    (details: ICategoriesProducts) => details.categories
);

export const categoriesErrorSelecor = createSelector(
    categoriesListFeature,
    (details: ICategoriesProducts) => details.error
);


export const categoriesListLoadingSelecor = createSelector(
    categoriesListFeature,
    (details: ICategoriesProducts) => details.loading
);