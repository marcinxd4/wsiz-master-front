import { IState, ICharmsState } from "../interface/redux.interface";
import { createSelector } from "@ngrx/store";

export const charmsFeature = (state: IState) => state.charms;

export const charmsSelector = createSelector(
    charmsFeature,
    (details: ICharmsState) => details.charms
);

export const charmsErrorSelector = createSelector(
    charmsFeature,
    (details: ICharmsState) => details.error
);

export const charmsLoadingSelector = createSelector(
    charmsFeature,
    (details: ICharmsState) => details.loading
);