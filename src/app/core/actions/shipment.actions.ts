import { createAction, props, union } from '@ngrx/store';
import { IShipment } from '../interface/shipment.interface';

export const getShipmentAction = createAction('[SHIPMENT] get shipment');
export const getShipmentSuccessAction = createAction('[SHIPMENT] get shipment success action', props<{ shipments: IShipment[] }>());
export const getShipmentFailureAction = createAction('[SHIPMENT] get shipment failure action', props<{ error: string }>());

export const allShipmentsUnion = union({
  getShipmentAction,
  getShipmentSuccessAction,
  getShipmentFailureAction
});
