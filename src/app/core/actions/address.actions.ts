import { createAction, props } from '@ngrx/store';

export const getUserAddressesAction = createAction('[USER_ADDRESS] get user addresses');
export const getUserAddressesSuccessAction = createAction('[USER_ADDRESS] get user addresses success', props<{ addresses: any[] }>());
export const getUserAddressesFailureAction = createAction('[USER_ADDRESS] get user addresses failure', props<{ error: string }>());
