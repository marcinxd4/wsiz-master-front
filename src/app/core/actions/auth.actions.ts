import { createAction, props } from '@ngrx/store';
import { AuthData } from '../../shared/interfaces/login.interface';

export const actionRefreshTokenAciton = createAction('[RefreshToken] refresh token');
export const actionRefreshTokenFailureAciton = createAction('[RefreshToken] refresh token failure', props<{ error: string }>());
export const actionRefreshTokenSuccessAciton = createAction('[RefreshToken] refresh token success', props<{ data: AuthData }>());
