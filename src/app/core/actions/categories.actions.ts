import { createAction, props } from '@ngrx/store';

export const getCategoriesListAction = createAction('[CategoriesList] get categories list action');
export const getCategoriesListSuccessActon = createAction(
  '[CategoriesList] get categories list success action', props<{ categories: any }>());
export const getCategoriesListFailureActon = createAction(
  '[CategoriesList] get categories list failure action', props<{ error: string }>());
